package florito.genetics;


public class Genetics {

	/**
	 * The main breeding method for creating next generation {@link Animal}s.
	 * <p>
	 * With this method you can breed a new child from one ore more parents. The mutationRate (0..1) gives the chance of a single bit in the 
	 * DNA byteArray to be mutated, with 0 being never and 1 being always.
	 * <p>
	 * Please make sure that the child as well as the parent objects are not null.
	 * <p>
	 * Programmers note: This method works the following way:
	 * <ol>
	 * <li> Registers the DNA ({@link Animal#registerDNA()}) on child and parent objects
	 * <li> Creates the byteArrays for the parent objects using {@link Animal#extractDNA()} and turns this into bitArrays
	 * <li> Crosses and mutates the bitArrays to create a child bitArray
	 * <li> Converts the child bitArray into a byteArray that gets inserted into the child with {@link Animal#insertDNA(byte[])}
	 * <li> Child gets initialised with {@link Animal#createStructureFromDNA()}
	 * </ol>
	 * <p> 
	 * @param child the child {@link Animal}
	 * @param mutationRate the mutation rate (0..1)
	 * @param parents the parent {@link Animal}
	 * @throws RuntimeException if child is <i>null</i>
	 * @throws RuntimeException if parent array is <i>null</i> or parent array length = 0
	 * @throws RuntimeException if one of the parents in the parent array is <i>null</i>
	 * @return index of parent per bit (-1 for mutation)
	 */
	public static int[] breed(Breedable child, float mutationRate, Breedable... parents) {
		if (child==null) {
			throw new RuntimeException("Cannot make a child that is null");
		}
		if (parents==null || parents.length==0) {
			throw new RuntimeException("Need at least one parent");
		}
		for (Breedable parent:parents) {
			if (parent==null) {
				throw new RuntimeException("Parents can not be null");
			}
		}
		
		// create parent byte arrays
		byte[][] parentByteArray = new byte[parents.length][];
		for (int i=0;i<parents.length;i++) {
			parentByteArray[i] = parents[i].extractDNA();
		}
		
		// create bit arrays
		boolean[][] parentBitArray = new boolean[parents.length][parentByteArray[0].length*8];
		for (int i=0;i<parents.length;i++) {
			int bit = 0;
			for (int j=0;j<parentByteArray[i].length;j++) {
				byte b = parentByteArray[i][j];
				parentBitArray[i][bit++] = ((b>>7)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>6)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>5)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>4)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>3)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>2)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>1)&0x01)==1;
				parentBitArray[i][bit++] = (b&0x01)==1;
			}
		}
				
		// cross and breed
		boolean[] childBitArray = new boolean[parentBitArray[0].length]; 
		int[] mutatedBits = new int[childBitArray.length];
		
		for (int i=0;i<parentBitArray[0].length;i++) {
			int selectedParent = (int)(Math.random()*parents.length);
			childBitArray[i] = parentBitArray[selectedParent][i];
			if (mutationRate>0 && Math.random()<mutationRate) {
				childBitArray[i] = !childBitArray[i];
				mutatedBits[i] = -1;
			} else {
				mutatedBits[i] = selectedParent;
			}
		}
		
		// create byte array
		byte[] childByteArray = new byte[childBitArray.length/8];
		int bit=0;
		for (int i=0;i<childByteArray.length;i++) {
			int b=0;
			b = b | (childBitArray[bit++]?0x80:0);
			b = b | (childBitArray[bit++]?0x40:0);
			b = b | (childBitArray[bit++]?0x20:0);
			b = b | (childBitArray[bit++]?0x10:0);
			b = b | (childBitArray[bit++]?0x08:0);
			b = b | (childBitArray[bit++]?0x04:0);
			b = b | (childBitArray[bit++]?0x02:0);
			b = b | (childBitArray[bit++]?0x01:0);
			childByteArray[i] = (byte)b;
		}
		
		child.insertDNA(childByteArray);
		
		return mutatedBits;
	}

	
	/**
	 * Randomises any Animal by giving it a random DNA string.
	 * <p>
	 * This method 
	 * <ol>
	 * <li> Calls the {@link Animal#registerDNA()} method.
	 * <li> Randomises the DNA byteArray ({@link #randomize(Breedable)}
	 * <li> Calls the {@link Animal#createStructureFromDNA()} method.
	 * </ol>
	 * @see #randomize(Breedable)
	 * @see Animal#registerDNA()
	 * @see Animal#createStructureFromDNA()
	 * @param animal
	 */
	public static void randomize(Animal animal) {
		animal.registerDNA();
		randomize((Breedable)animal);
		animal.createStructureFromDNA();
	}
	
	/**
	 * Randomizes any Breedable object by giving it a random DNA string
	 * @param breedable
	 * @see #randomize(Animal)
	 */
	public static void randomize(Breedable breedable) {
		byte[] byteArray = new byte[breedable.size()];
		for (int i=0;i<byteArray.length;i++) {
			byteArray[i] = (byte)(Math.random()*256);
		}
		breedable.insertDNA(byteArray);
	}


	/**
	 * The main breeding method for creating next generation {@link Animal}s.
	 * <p>
	 * With this method you can breed a new child from one ore more parents. The mutationRate (0..1) gives the chance of a single bit in the 
	 * DNA byteArray to be mutated, with 0 being never and 1 being always.
	 * <p>
	 * Please make sure that the child as well as the parent objects are not null.
	 * <p>
	 * Programmers note: This method works the following way:
	 * <ol>
	 * <li> Registers the DNA ({@link Animal#registerDNA()}) on child and parent objects
	 * <li> Creates the byteArrays for the parent objects using {@link Animal#extractDNA()} and turns this into bitArrays
	 * <li> Crosses and mutates the bitArrays to create a child bitArray
	 * <li> Converts the child bitArray into a byteArray that gets inserted into the child with {@link Animal#insertDNA(byte[])}
	 * <li> Child gets initialised with {@link Animal#createStructureFromDNA()}
	 * </ol>
	 * <p> 
	 * @param child the child {@link Animal}
	 * @param mutationRate the mutation rate (0..1)
	 * @param parents the parent {@link Animal}
	 * @throws RuntimeException if child is <i>null</i>
	 * @throws RuntimeException if parent array is <i>null</i> or parent array length = 0
	 * @throws RuntimeException if one of the parents in the parent array is <i>null</i>
	 */
	public static void breed(Animal child, float mutationRate, Animal... parents) {
		if (child==null) {
			throw new RuntimeException("Cannot make a child that is null");
		}
		if (parents==null || parents.length==0) {
			throw new RuntimeException("Need at least one parent");
		}
		for (Animal parent:parents) {
			if (parent==null) {
				throw new RuntimeException("Parents can not be null");
			}
		}
		
		child.registerDNA();
		for (int i=0;i<parents.length;i++) {
			parents[i].registerDNA();
		}
		
		breed((Breedable)child, mutationRate, parents);
		
		/*
		
		// create parent byte arrays
		byte[][] parentByteArray = new byte[parents.length][];
		for (int i=0;i<parents.length;i++) {
			parentByteArray[i] = parents[i].extractDNA();
		}
		
		// create bit arrays
		boolean[][] parentBitArray = new boolean[parents.length][parentByteArray[0].length*8];
		for (int i=0;i<parents.length;i++) {
			int bit = 0;
			for (int j=0;j<parentByteArray[i].length;j++) {
				byte b = parentByteArray[i][j];
				parentBitArray[i][bit++] = ((b>>7)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>6)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>5)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>4)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>3)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>2)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>1)&0x01)==1;
				parentBitArray[i][bit++] = (b&0x01)==1;
			}
		}
				
		// cross and breed
		boolean[] childBitArray = new boolean[parentBitArray[0].length]; 
		for (int i=0;i<parentBitArray[0].length;i++) {
			int selectedParent = (int)(Math.random()*parents.length);
			childBitArray[i] = parentBitArray[selectedParent][i];
			if (mutationRate>0 && Math.random()<mutationRate) {
				childBitArray[i] = !childBitArray[i];
			}
		}
		
		// create byte array
		byte[] childByteArray = new byte[childBitArray.length/8];
		int bit=0;
		for (int i=0;i<childByteArray.length;i++) {
			int b=0;
			b = b | (childBitArray[bit++]?0x80:0);
			b = b | (childBitArray[bit++]?0x40:0);
			b = b | (childBitArray[bit++]?0x20:0);
			b = b | (childBitArray[bit++]?0x10:0);
			b = b | (childBitArray[bit++]?0x08:0);
			b = b | (childBitArray[bit++]?0x04:0);
			b = b | (childBitArray[bit++]?0x02:0);
			b = b | (childBitArray[bit++]?0x01:0);
			childByteArray[i] = (byte)b;
		}
		
		child.insertDNA(childByteArray);
		
		*/
		
		child.createStructureFromDNA();
	}
	
	
	
}
