package florito.genetics;

/**
 * The Breedable interface
 * <p>
 * This interface is used for creating breedable objects. This means turning the genotype (the DNA) into the phenotype (the physical appearance)
 * of this object. The DNA is represented as a byte array. How the object is converted from genotype into phenotype is up to the programmer.
 * For this the following two methods need to be implemented:
 * <ul>
 * <li> {@link #extractDNA()}
 * <li> {@link #insertDNA(byte[])}
 * </ul>
 * Also the method {@link #randomizeDNA()} needs to be added. It will be useful for creating first-generation objects.
 * <p>
 * @author Marcus
 *
 */
public interface Breedable {

	/**
	 * Creates and returns a byte array representing this Breedable's DNA part.
	 * @return
	 */
	public byte[] extractDNA();
	
	/**
	 * Creates the DNA for this Breedable from a byte array. <p>
	 * This method should also handle any wrongly allocated byteArray. It is up to the programmer to catch any false arrays.
	 * @param byteArray
	 */
	public void insertDNA(byte[] byteArray);
	
	/**
	 * Randomizes the DNA of this Breedable object.
	 */
	//public void randomizeDNA();
	
	/**
	 * Returns the size of the byteArray representing the DNA string.
	 */
	public int size();
	
}
