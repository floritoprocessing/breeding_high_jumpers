package florito.genetics;

public class GeneticUtils {
	private static final int MASK = 0xff;

	/**
	 * Creates a String representation of this byte array.
	 * @param byteArray
	 * @return
	 */
	public static String byteArrayToBitString(byte[] byteArray) {
		StringBuilder sb = new StringBuilder();
		if (byteArray == null) {
			throw new IllegalArgumentException("byteArray must not be null");
		}
		for (int i=0;i<byteArray.length;i++) {
			int asInt = (int)(byteArray[i]);
			if (asInt<0) {
				asInt+=256;
			}
			String oneByte = Integer.toBinaryString(asInt);
			while (oneByte.length()<8) {
				oneByte = "0"+oneByte;
			}
			sb.append(oneByte);
			if (i<byteArray.length-1) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
	
	/**
	 * Convert byte array (of size 4) to float
	 * @param byteArray
	 * @return
	 */
	public static float byteArrayToFloat(byte[] byteArray) {
		return Float.intBitsToFloat(byteArrayToInt(byteArray));
	}

	/**
	 * Convert byte array (of size 4) to int
	 * @param byteArray
	 * @return
	 */
	public static int byteArrayToInt(byte[] byteArray) {
		int bits = 0;
		int i = 0;
		for (int shifter = 3; shifter >= 0; shifter--) {
			bits |= ((int) byteArray[i] & MASK) << (shifter * 8);
			i++;
		}
		return bits;
	}

	/**
	 * Convert byte array to String.
	 * @param byteArray
	 * @return
	 */
	public static String byteArrayToIntString(byte[] byteArray) {
		StringBuilder sb = new StringBuilder("[");
		if (byteArray == null) {
			throw new IllegalArgumentException("byteArray must not be null");
		}
		int arrayLen = byteArray.length;
		for (int i = 0; i < arrayLen; i++) {
			sb.append(byteArray[i]);
			if (i == arrayLen - 1) {
				sb.append("]");
			} else {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	/**
	 * Convert float to byte array (of size 4)
	 * 
	 * @param f
	 * @return
	 */
	public static byte[] floatToByteArray(float f) {
		int i = Float.floatToRawIntBits(f);
		return intToByteArray(i);
	}
	
	/**
	 * Convert int to byte array (of size 4)
	 * 
	 * @param param
	 * @return
	 */
	public static byte[] intToByteArray(int param) {
		byte[] result = new byte[4];
		for (int i = 0; i < 4; i++) {
			int offset = (result.length - 1 - i) * 8;
			result[i] = (byte) ((param >>> offset) & MASK);
		}
		return result;
	}
}