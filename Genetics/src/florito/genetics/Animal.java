package florito.genetics;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * The Animal class.<br>
 * By default all public non-final variables are the breedable ones 
 * @author Marcus
 *
 */
public abstract class Animal implements Breedable, Comparable<Animal> {

	private float fitness = 0;

	private Breedable[] DNA;
	
	
	/**
	 * Sets the fitness.<P>
	 * Higher values mean higher fitness.
	 * @param fitness
	 */
	public void setFitness(float fitness) {
		this.fitness = fitness;
	}
	
	
	/**
	 * Retrieves the fitness.<p>
	 * Higher values mean higher fitness. 
	 * @return the fitness of this Animal
	 */
	public float getFitness() {
		return fitness;
	}
	
	
	
	
	/**
	 * Creates the structure of this Animal from its DNA. 
	 * <p>
	 * After setting the DNA from a byte array (see {@link #insertDNA(byte[])}) or using the method {@link #randomizeDNA()},
	 * the structure of the Animal needs to be created.
	 * Think of things like initialising variables, positions, deductable objects, etc..
	 * <p>
	 * When using the {@link Genetics#breed(Animal, float, Animal...)} to breed new generations, 
	 * this method gets called on all new children automatically.
	 */
	public abstract void createStructureFromDNA();
	
	
	public void registerDNA() {		
		ArrayList<Breedable> dna = new ArrayList<Breedable>();
		
		Class<?> theClass = this.getClass();
		Field[] fields = theClass.getFields();

		/*
		 * Go through all public fields
		 */
		for (Field field:fields) {
			
			// the field...
			Class<?> fieldClass = field.getType();
			
			// ... and its interfaces
			Class<?>[] fieldClassInterfaces = fieldClass.getInterfaces();
			
			/*
			 * Go through all interfaces of each field, ...
			 */
			for (Class<?> fieldClassInterface:fieldClassInterfaces) {
				
				/*
				 * ...check if the field implements Breedable...
				 */
				if (fieldClassInterface.equals(Breedable.class)) {
					
					/*
					 * ... and add the field to the breedable variables
					 */
					try {
						
						Breedable fieldVar = (Breedable)field.get(this);
						dna.add(fieldVar);
						
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					
				}
			}
			
		}
		
		DNA = dna.toArray(new Breedable[0]);
		
	}

	@Override
	public byte[] extractDNA() {
		
		
		byte[][] arrs = new byte[DNA.length][];
		int totalSize = 0;
		for (int i=0;i<DNA.length;i++) {
			arrs[i] = DNA[i].extractDNA();
			totalSize += arrs[i].length;
		}
		
		byte[] out = new byte[totalSize];
		int offset = 0;
		for (int i=0;i<arrs.length;i++) {
			System.arraycopy(arrs[i], 0, out, offset, arrs[i].length);
			offset += arrs[i].length;
		}
		
		return out;
	}

	@Override
	public void insertDNA(byte[] byteArray) {
		
		byte[][] arrs = new byte[DNA.length][];
		int totalSize = 0;
		int[] arrayLength = new int[DNA.length];
		for (int i=0;i<DNA.length;i++) {
			arrs[i] = DNA[i].extractDNA();
			arrayLength[i] = arrs[i].length;
			totalSize += arrs[i].length;
		}
		
		int srcPos = 0;
		for (int i=0;i<DNA.length;i++) {
			byte[] subByteArray = new byte[arrayLength[i]]; 
			System.arraycopy(byteArray, srcPos, subByteArray, 0, arrayLength[i]);
			DNA[i].insertDNA(subByteArray);
			srcPos += subByteArray.length;
		}
		
	}
	
	/**
	 * Compares one Animal to another for fitness sorting.
	 * <p>
	 * When sorting an array of Animals, the ones with the highest fitness value end up first.
	 */
	@Override
	public int compareTo(Animal o) {
		if (o.fitness<fitness) {
			return -1;
		} else if (o.fitness>fitness) {
			return 1;
		} else {
			return 0;
		}
	}
	
	
	
}
