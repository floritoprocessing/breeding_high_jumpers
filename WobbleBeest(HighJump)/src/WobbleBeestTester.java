import java.util.ArrayList;
import java.util.Arrays;

import javax.management.MXBean;

import processing.core.PApplet;
import traer.physics.ParticleSystem;
import traer.physics.Vector3D;
import florito.genetics.Genetics;


public class WobbleBeestTester extends PApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2314755680147457497L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"WobbleBeestTester"});
	}
	
	final int w=100;
	final int h=2;
	Wobblebeest[] generation = new Wobblebeest[w*h];
	Wobblebeest[] sortedGeneration = new Wobblebeest[w*h];
	
	final float scaleFac = 25.0f;
	final float xTranslation = 0;
	final float yTranslation = 120;
	
	//final int cyclesBeforeEvaluation = 343;//171; // 250
	long generationStartMs;
	final int msBeforeEvaluation = 1000;//6857;
	
	final int winnersForNextGeneration = 15;
	final int randomForNextGeneration = 5;
	final float mutationRate = 0.001f;
	
	private ParticleSystem physics = new ParticleSystem(0.1f,0.01f);
	
	ArrayList<Wobblebeest> winners = new ArrayList<Wobblebeest>();
	ArrayList<Float> averageJumpHeightPerGeneration = new ArrayList<Float>();
	
	int cycles = 0;
	
	boolean saveFrames = false;
	int saveNthFrame = 2;
	String saveLocation = "D:\\- Projects\\Strandbeest\\WobbleBeest(HighJump)Output\\WobbleBeestHighJump_######.bmp";
	String lastStat = "";
	
	
	public void settings() {
		size(1024,768,P3D);
	}
	
	public void setup() {
		
		smooth();
		
		textFont(loadFont("Consolas-Bold-16.vlw"));
		for (int i=0;i<generation.length;i++) {
			generation[i] = new Wobblebeest(physics);
			Genetics.randomize(generation[i]);
			sortedGeneration[i] = generation[i];
		}
		
		generationStartMs = System.currentTimeMillis();
		frameRate(300);
	}
	
	public void draw() {
		
		long frameTime = System.currentTimeMillis();
		
		background(240);
		
		if (frameCount==1) {
			frame.setLocation(1920-3, -24);
		}
		
		fill(0);
		//println(frameRate);
		/*
		 * Tick generation
		 */
		for (int i=0;i<generation.length;i++) {
			generation[i].tick(5.0f);
		}
		physics.tick();
		
		
		/*
		 * Evaluate fitness
		 */
		for (Wobblebeest beest:generation) {
			if (beest.isHasHitFloor()) {
				Vector3D pos = beest.getCenterPosition();
				if (pos.y()<beest.getHighestPosition()) {
					beest.setHighestPosition(pos.y());
				}
				beest.setFitness(-beest.getHighestPosition());
			}
		}
		Arrays.sort(sortedGeneration);
		
		
		if (averageJumpHeightPerGeneration.size()>0) {
			averageJumpHeightPerGeneration.remove(averageJumpHeightPerGeneration.size()-1);
		}
		float averageJump = getAverageJumpHeight();
		averageJumpHeightPerGeneration.add(new Float(-averageJump));
		
		
		/*
		 * Draw everything
		 */
		drawAnimals();
		
		drawJumpHeights();
		
		drawStatusBar(frameTime);
		
		drawAverageHeightStats();
		
		fill(64,0,0);
		text(lastStat,170,26);
		
		if (saveFrames && frameCount%saveNthFrame==0) {
			saveFrame(saveLocation);
		}
		
		
		cycles++;
		
		//if (cycles>=cyclesBeforeEvaluation) {
			
		if (frameTime>=generationStartMs+msBeforeEvaluation) {
			cycles=0;
			
			//averageJumpHeightPerGeneration.remove(averageJumpHeightPerGeneration.size()-1);
			averageJump = getAverageJumpHeight();
			averageJumpHeightPerGeneration.add(new Float(-averageJump));
			
			String stat = "Highest "+winnersForNextGeneration+" jumpers: " +
					nf(-sortedGeneration[winnersForNextGeneration-1].getHighestPosition(),1,1)+
					" .. "+nf(-sortedGeneration[0].getHighestPosition(),1,1)+
					" (Average jump height: "+nf(-averageJump,1,1)+")";
			
			lastStat = stat;
			println(stat);
				
			fill(255,0,0);
			
			
			
			createNewGeneration();
			generationStartMs = System.currentTimeMillis();
		}
		
		//current
		
	}

	private float getAverageJumpHeight() {
		float averageJump = 0;
		for (int i=0;i<winnersForNextGeneration;i++) {
			winners.add(sortedGeneration[i]);
			averageJump += sortedGeneration[i].getHighestPosition();
		}
		averageJump /= winnersForNextGeneration;
		return averageJump;
	}

	private void drawAverageHeightStats() {
		
		float maxHeight = getHighestAverageJumpHeight();
		
		
		if (maxHeight!=0) {
			pushMatrix();
			translate(10,height-10);
			
			
			float xStep = 20;
			float scaleFac = -20/maxHeight;
			
			noFill();
			
			stroke(128,0,0);
			strokeWeight(1);
			beginShape();
			for (int i=0;i<averageJumpHeightPerGeneration.size();i++) {
				vertex(i*xStep,averageJumpHeightPerGeneration.get(i)*scaleFac);
			}
			endShape();
			
			stroke(128,0,0);
			strokeWeight(4);
			beginShape(POINTS);
			for (int i=0;i<averageJumpHeightPerGeneration.size();i++) {
				vertex(i*xStep,averageJumpHeightPerGeneration.get(i)*scaleFac);
			}
			endShape();
			
			stroke(0,64);
			strokeWeight(1);
			beginShape(LINES);
			for (int i=0;i<averageJumpHeightPerGeneration.size();i++) {
				vertex(i*xStep,0);
				vertex(i*xStep,averageJumpHeightPerGeneration.get(i)*scaleFac);
			}
			endShape();
			
			
			
			popMatrix();
		}
		
	}

	private float getHighestAverageJumpHeight() {
		float maxHeight = 0;
		for (int i=0;i<averageJumpHeightPerGeneration.size();i++) {
			maxHeight = Math.max(maxHeight,averageJumpHeightPerGeneration.get(i));
		}
		return maxHeight;
	}

	private void drawStatusBar(long frameTime) {
		strokeWeight(1);
		noStroke();
		fill(255,0,0,64);
		
		//float percentage = (float)cycles/cyclesBeforeEvaluation;
		float percentage = (frameTime-generationStartMs) / (float)msBeforeEvaluation;
		//println(frameTime+" "+generationStartMs+" , "+percentage);
		
		rect(10,10,100*percentage,20);
		stroke(0,64);
		fill(0,32);
		rect(10,10,100,20);
	}

	private void drawJumpHeights() {
		int i=0;
		float largest = Math.max(w,h);
		for (int y=0;y<h;y++) {
			for (int x=0;x<w;x++) {
				
				boolean isBest = false;
				for (int sorted=0;sorted<winnersForNextGeneration;sorted++) {
					if (generation[i].equals(sortedGeneration[sorted])) {
						isBest = true;
					}
				}
				
				pushMatrix();
				
				translate((x+0.5f)/w*width + xTranslation,(y+0.5f)/h*height + yTranslation);
				scale(scaleFac);
				scale(1.2f/largest, 1.2f/largest);
				if (isBest) {
					strokeWeight(3);
					stroke(128,0,0);
					line(-100, generation[i].getHighestPosition(), 100, generation[i].getHighestPosition());
					strokeWeight(1);
					stroke(128,0,0,64);
					line(0, generation[i].getHighestPosition(), 0, 0);
					//Vector3D pos = generation[i].getCenterPosition();
					//line(0, generation[i].getHighestPosition(), pos.x(), pos.y());
				} else {
					strokeWeight(1);
					stroke(0,0,0,32);
					line(-100, generation[i].getHighestPosition(), 100, generation[i].getHighestPosition());
					stroke(0,0,0,16);
					line(0, generation[i].getHighestPosition(), 0, 0);
				}
				
				
				
				
				popMatrix();
				i++;
			}
		}
	}

	private void drawAnimals() {
		int i=0;
		float largest = Math.max(w,h);
		for (int y=0;y<h;y++) {
			for (int x=0;x<w;x++) {
				
				pushMatrix();
				translate((x+0.5f)/w*width + xTranslation,(y+0.5f)/h*height + yTranslation);
				
				scale(scaleFac);
				scale(1.2f/largest, 1.2f/largest);
				generation[i].draw(this,-200,200);
				
				Vector3D pos = generation[i].getCenterPosition();
				ellipseMode(RADIUS);
				stroke(128,0,0,32);
				ellipse(pos.x(), pos.y(), 80, 80);
				
				popMatrix();
				
				i++;
			}
		}
	}
	
	private void createNewGeneration() {
		
		//System.out.println("Creating new generation after "+cycles+" cycles.");
		//cycles = 0;

		/*
		 * Add random creatures to winners
		 */
		if (randomForNextGeneration>0) {
			for (int i=0;i<randomForNextGeneration;i++) {
				int index = (int)(Math.random()*generation.length);
				winners.add(generation[index]);
			}
		}
		
		/*
		 * Remove old generation
		 */
		for (Wobblebeest beest:generation) {
			beest.removeFromPhysics();
		}
		
		
		/*
		 * Create new generation
		 */
		Wobblebeest[] nextGeneration = new Wobblebeest[w*h];
		for (int i=0;i<nextGeneration.length;i++) {			
			Wobblebeest child = new Wobblebeest(physics);
			Wobblebeest parent0 = winners.get((int)(Math.random()*winners.size()));
			Wobblebeest parent1 = parent0;
			while (parent1==parent0) {
				parent1 = winners.get((int)(Math.random()*winners.size()));
			}
			
			//System.out.println("------------");
			
			Genetics.breed(child, mutationRate, new Wobblebeest[] {parent0,parent1});
			//child.createPhysics();
			//System.out.println("parent0: "+Genetics.asBinaryString(parent0));
			//System.out.println("parent1: "+Genetics.asBinaryString(parent1));		
			//System.out.println("child:   "+Genetics.asBinaryString(child));
			
			nextGeneration[i] = child;
		}
		
		/*
		 * Replace old generation
		 */
		generation = nextGeneration;
		
		for (int i=0;i<generation.length;i++) {
			sortedGeneration[i] = generation[i];
		}
		
		winners.clear();
		
	}
	
}
