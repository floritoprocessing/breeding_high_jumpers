package processinggeneticstester;

import java.util.Arrays;

import florito.genetics.Animal;
import florito.genetics.Breedable;
import florito.genetics.Genetics;
import florito.genetics.Primitive;
import processing.core.PApplet;


public class ProcessingGeneticsTester extends PApplet {

	public static void main(String[] args) {
		PApplet.main("processinggeneticstester.ProcessingGeneticsTester");
	}
	
	
	public class Radius implements Breedable {

		byte DNA;
		
		public Radius() {
		}
		
		@Override
		public byte[] extractDNA() {
			return new byte[] {DNA};
		}

		@Override
		public void insertDNA(byte[] byteArray) {
			DNA = byteArray[0];
		}

		@Override
		public int size() {
			return 1;
		}
		
		public int getValue() {
			int val = (int)DNA;
			if (val<0) {
				val+=256;
			}
			return val;
		}
		
	}
	
	public class CircleAnimal extends Animal {

		public Radius number;
		
		public CircleAnimal() {
			number = new Radius();
			Genetics.randomize(number);
		}
		
		public void createStructureFromDNA() {
		}
		
		public int size() {
			return number.size();
		}
		
		public int getRadius() {
			return number.getValue();
		}
		
		public String toString() {
			return ""+number.getValue();
		}
		
	}

	
	CircleAnimal[] generation = new CircleAnimal[400];
	
	public void settings() {
		size(500,500,P3D);
	}
	
	public void setup() {
		
		
		for (int i=0;i<generation.length;i++) {
			generation[i] = new CircleAnimal();
		}
		
		calculateFitness();
		//Arrays.sort(generation);
		showGeneration();
	}
	
	public void mousePressed() {
		// Sort NumberAnimals by fitness
		Arrays.sort(generation);
		
		// Create new empty generation
		CircleAnimal[] newGeneration = new CircleAnimal[generation.length];
		
		// Create new generation
		for (int i=0;i<newGeneration.length;i++) {
			
			// choose from best 5%
			int index0 = (int)random(generation.length*0.05f);
			int index1 = index0;
			while (index0==index1) {
				index1 = (int)random(generation.length*0.05f);
			}
			CircleAnimal[] parents = new CircleAnimal[] {
					generation[index0],
					generation[index1]
			};
			
			CircleAnimal child = new CircleAnimal();
			
			// create new child
			Genetics.breed(child, 0.01f, parents);
		
			newGeneration[i] = child;
			
		}
		
		// Replace old with new generation
		generation = newGeneration;
		
		// Evaluate fitness
		calculateFitness();
		
		// and show
		showGeneration();
	}
	
	public void calculateFitness() {
		for (int i=0;i<generation.length;i++) {
			int distanceTo50 = abs(generation[i].getRadius()-50);
			
			float fitness = 0;
			if (distanceTo50!=0) {
				fitness = 1.0f/distanceTo50;
			} else {
				fitness = Float.MAX_VALUE;
			}
			generation[i].setFitness(fitness);
		}
	}

	public void showGeneration() {
		for (int i=0;i<generation.length;i++) {
			print(generation[i]+"("+generation[i].getFitness()+")"+", ");
			if (i>0&&(i%10==9)) {
				println();
			}
		}
		println();
		println();
	}
	
	public void drawGeneration() {
		for (int i=0;i<generation.length;i++) {
			float y = i/20;
			float x = i%20;
			x *= 25;
			y *= 25;
			ellipseMode(RADIUS);
			float radius = generation[i].getRadius()*0.1f;
			ellipse(x,y,radius,radius);
		}
	}
	
	public void draw() {
		background(255);
		drawGeneration();
	}
}
