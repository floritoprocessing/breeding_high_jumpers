import florito.genetics.Breedable;

/**
 * LegSize: 1..256
 * @author Marcus
 *
 */
public class Length implements Breedable {

	private int size = 1;
	
	public Length() {
	}
	
	public Length(int size) {
		this.size = Math.max(1, Math.min(256, size));
	}
	
	public void setSize(int size) {
		this.size = Math.max(1, Math.min(256, size));
	}
	
	public int getSize() {
		return size;
	}
	
	@Override
	public void fromByteArray(byte[] byteArray) {
		size = (int)byteArray[0];
		if (size<0) {
			size +=256;
		}
	}
	
	@Override
	public byte[] toByteArray() {
		return new byte[] {(byte)size};
	}

	@Override
	public boolean isBreedable() {
		return true;
	}

	@Override
	public void setBreedable(boolean breedable) {
	}

	

}
