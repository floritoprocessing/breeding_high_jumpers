import processing.core.PApplet;
import traer.physics.ParticleSystem;


public class Strandbeest extends PApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2314755680147457497L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"Strandbeest"});
	}
	
	int w=8;
	int h=8;
	Leg[] beesten = new Leg[w*h];
	
	private final ParticleSystem physics = new ParticleSystem(0,0.1f);
	
	public void setup() {
		size(800,800,P3D);
	
		for (int i=0;i<beesten.length;i++) {
			beesten[i] = new Leg(physics);
			beesten[i].randomize();
		}
		
	}
	
	public void draw() {
		background(240);
		
		int i=0;
		for (int y=0;y<h;y++) {
			for (int x=0;x<w;x++) {
				
				pushMatrix();
				translate((x+0.5f)/w*width,(y+0.5f)/h*height);
				scale(1.2f/w, 1.2f/h);
				beesten[i].draw(this);
				popMatrix();
				
				i++;
			}
		}
		
		for (i=0;i<beesten.length;i++) {
			beesten[i].rotate(0.15f);
		}
		
		physics.tick();
		
		for (i=0;i<beesten.length;i++) {
			//beesten[i].checkForSpringIntersection();
		}
	}
	
}
