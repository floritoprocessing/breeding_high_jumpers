import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import processing.core.PApplet;
import traer.physics.Particle;
import traer.physics.ParticleSystem;
import traer.physics.Spring;
import traer.physics.Vector3D;
import florito.genetics.Animal;
import florito.genetics.Breedable;

public class Leg extends Animal {

	private final ParticleSystem physics;
	
	public HalfLength pivotToALength = new HalfLength();
	public HalfLength pivotToDLengthPlusA = new HalfLength();
	
	public Length AtoB = new Length();
	public Length BtoD = new Length();
	
	public Length BtoC = new Length();
	public Length CtoD = new Length();
	
	
	private Particle pivot, A, B, C, D, E, F, G;
	private Spring pivotToA, ab, bd, bc, cd;
	private Spring[] allSprings;
	
	private float pivotRotation = 0f;
	private boolean springIntersected = false;
	
	private static final float mass = 1f;
	private static final float strength = 1f;
	private static final float damping = 1f;
	
	private final int legID;
	
	public Leg(ParticleSystem physics) {
		this.physics = physics;
		defineBreedableVars();
		legID = (int)(Integer.MAX_VALUE*Math.random());
	}
	
	public void randomize() {
		
		boolean physicsOk = false;
		
		while (!physicsOk) {
			
			physics.removeParticle(pivot);
			physics.removeParticle(A);
			physics.removeParticle(B);
			physics.removeParticle(C);
			physics.removeParticle(D);
			physics.removeParticle(E);
			physics.removeParticle(F);
			physics.removeParticle(G);
			if (allSprings!=null) {
				for (Spring s:allSprings) {
					physics.removeSpring(s);
				}
			}
			
			for (Breedable b:allBreedableVars) {
				byte[] byteArray = b.toByteArray();
				for (int i=0;i<byteArray.length;i++) {
					byteArray[i] = (byte)(Math.random()*256);
				}
				b.fromByteArray(byteArray);
			}
			
			physicsOk = createPhysics();
			
		}
		
		
	}
	
	/**
	 * Returns false if the physics failed creating (due to wrong distances)
	 * @return
	 */
	private boolean createPhysics() {
		ArrayList<Spring> springList = new ArrayList<Spring>();
		boolean physicsOk = true;
		
		pivot = physics.makeParticle(mass, 0, 0, 0);
		pivot.makeFixed();
		
		A = physics.makeParticle(mass, pivotToALength.getSize(), 0, 0);
		A.makeFixed();
		
		pivotToA = physics.makeSpring(pivot, A, 0, 0, pivotToALength.getSize());
		//springList.add(pivotToA);
		
		D = physics.makeParticle(mass, -pivotToALength.getSize()-pivotToDLengthPlusA.getSize(), 0, 0);
		D.makeFixed();
		
		Vector3D[] possibleB = intersectionTwoCircles(D.position(), A.position(), BtoD.getSize(), AtoB.getSize());
		if (possibleB!=null) {
			Vector3D pointB = getUpper(possibleB);
			B = physics.makeParticle(mass, pointB.x(), pointB.y(), 0);
			ab = physics.makeSpring(A, B, strength, damping, AtoB.getSize());
			bd = physics.makeSpring(B, D, strength, damping, BtoD.getSize());
			springList.add(ab);
			springList.add(bd);
			
			Vector3D[] possibleC = intersectionTwoCircles(B.position(), D.position(), BtoC.getSize(), CtoD.getSize());
			if (possibleC!=null) {
				Vector3D pointC = getFurthestFrom(possibleC, A.position());
				C = physics.makeParticle(mass, pointC.x(), pointC.y(), 0);
				bc = physics.makeSpring(B, C, strength, damping, BtoC.getSize());
				cd = physics.makeSpring(C, D, strength, damping, CtoD.getSize());
				springList.add(bc);
				springList.add(cd);
			} else {
				physicsOk = false;
			}
			
		} else {
			physicsOk = false;
		}
		
		allSprings = springList.toArray(new Spring[0]);
		
		return physicsOk;
	}
	
	private static Vector3D getUpper(Vector3D[] twoPoints) {
		if (twoPoints.length==1) {
			return twoPoints[0];
		}
		if (twoPoints[0].y()<twoPoints[1].y()) {
			return twoPoints[0];
		} else {
			return twoPoints[1];
		}
	}
	
	private static Vector3D getLeft(Vector3D[] twoPoints) {
		if (twoPoints.length==1) {
			return twoPoints[0];
		}
		if (twoPoints[0].x()<twoPoints[1].x()) {
			return twoPoints[0];
		} else {
			return twoPoints[1];
		}
	}
	
	private static Vector3D getFurthestFrom(Vector3D[] twoPoints, Vector3D P) {
		
		if (twoPoints.length==1) {
			return twoPoints[0];
		} else {
			float d0 = twoPoints[0].distanceSquaredTo(P);
			float d1 = twoPoints[1].distanceSquaredTo(P);
			if (d0>d1) {
				return twoPoints[0];
			} else {
				return twoPoints[1];
			}
		}
		
	}
	
	private Vector3D[] intersectionTwoCircles(Vector3D P0, Vector3D P1, float r0, float r1) {
		float d = P1.distanceTo(P0);
		if (d>r0+r1) {
			return null; // no solution: the circles are separate
		}
		else if (d<Math.abs(r0-r1)) {
			return null; // no solution: one circle is contained within the other
		}
		else if (d==0 && r0==r1) {
			return null; // circles are coincident -> infinite number of solutions
		}
		else {
			//http://local.wasp.uwa.edu.au/~pbourke/geometry/2circle/
			float x0 = P0.x();
			float y0 = P0.y();
			float x1 = P1.x();
			float y1 = P1.y();
			
			float a = (r0*r0-r1*r1+d*d)/(2*d);
			
			float h = (float)Math.sqrt(r0*r0-a*a);
			
			Vector3D P1minP0 = new Vector3D(P1);
			P1minP0.subtract(P0);
			Vector3D P1minP0MulADivD = new Vector3D(P1minP0);
			P1minP0MulADivD.multiplyBy(a/d);
			
			Vector3D P2 = new Vector3D(P0);
			P2.add(P1minP0MulADivD);
			
			float x2 = P2.x();
			float y2 = P2.y();
			
			float x3_0 = x2 + h * (y1-y0)/d;
			float x3_1 = x2 - h * (y1-y0)/d;
			float y3_0 = y2 + h * (x1-x0)/d;
			float y3_1 = y2 - h * (x1-x0)/d;
			
			if (x3_0==x3_1 && y3_0==y3_1) {
				return new Vector3D[] { new Vector3D(x3_0,y3_0,0) };
			} else {
				return new Vector3D[] { new Vector3D(x3_0,y3_0,0), new Vector3D(x3_1,y3_1,0) };
			}
		}
	}

	public void draw(PApplet pa) {
		pa.noFill();
		pa.noStroke();
		pa.ellipseMode(PApplet.RADIUS);
		
		float r = 15;
		pa.fill(0,0,0);
		particleCircle(pa, pivot, r);
		pa.fill(128,0,0);
		particleCircle(pa, A, r);
		particleCircle(pa, D, r);
		
		pa.noFill();
		pa.stroke(128,0,0);
		springLine(pa, pivotToA);
		//particleLine(pa, pivot, A);
		
		if (B!=null) {
			pa.noFill();
			pa.stroke(0,0,0);
			particleCircle(pa, B, r);
			springLine(pa, ab);
			springLine(pa, bd);
			
			if (C!=null) {
				particleCircle(pa, C, r);
				springLine(pa, bc);
				springLine(pa, cd);
			}
			
		}
		
	}
	
	public void rotate(float rad) {
		if (!springIntersected) {
			pivotRotation -= rad;
			float x = (float)(Math.cos(pivotRotation)*pivotToALength.getSize());
			float y = (float)(Math.sin(pivotRotation)*pivotToALength.getSize());
			A.position().set(x,y,0);
		}
	}
	
	public void checkForSpringIntersection() {
		if (!springIntersected) {
			int len = allSprings.length;
			for (int i=0;i<len-1;i++) {
				for (int j=i+1;j<len;j++) {
					//System.out.println("test "+i+" adn "+j);
					if (innerIntersection(allSprings[i],allSprings[j])) {
						springIntersected = true;
						return;
					}
				}
			}
		}
	}
	
	private static boolean innerIntersection(Spring s1, Spring s2) {
		
		//System.out.println("Testing intersection on "+legID);
		
		float s1x0 = s1.getOneEnd().position().x();
		float s1y0 = s1.getOneEnd().position().y();
		float s1x1 = s1.getTheOtherEnd().position().x();
		float s1y1 = s1.getTheOtherEnd().position().y();
		
		float s2x0 = s2.getOneEnd().position().x();
		float s2y0 = s2.getOneEnd().position().y();
		float s2x1 = s2.getTheOtherEnd().position().x();
		float s2y1 = s2.getTheOtherEnd().position().y();
		
		float s1dx = s1x1-s1x0;
		float s1dy = s1y1-s1y0;
		float s2dx = s2x1-s2x0;
		float s2dy = s2y1-s2y0;
		
		// short so that there are no point overlaps
		s1dx *= 0.99f;
		s1dy *= 0.99f;
		s2dx *= 0.99f;
		s2dy *= 0.99f;
		
		Point2D.Float P0 = new Point2D.Float(s1x0,s1y0);
		Point2D.Float P1 = new Point2D.Float(s1x0+s1dx,s1y0+s1dy);
		Point2D.Float P2 = new Point2D.Float(s2x0,s2y0);
		Point2D.Float P3 = new Point2D.Float(s2x0+s2dx,s2y0+s2dy);
		
		Line2D.Float line0 = new Line2D.Float(P0, P1);
		Line2D.Float line1 = new Line2D.Float(P2, P3);
		
		return line0.intersectsLine(line1);
	}
	
	private static void particleCircle(PApplet pa, Particle p, float radius) {
		pa.ellipse(p.position().x(), p.position().y(), radius, radius);
	}
	
	private static void particleLine(PApplet pa, Particle a, Particle b) {
		pa.line(a.position().x(), a.position().y(), b.position().x(), b.position().y());
	}
	
	private static void springLine(PApplet pa, Spring s) {
		pa.line(s.getOneEnd().position().x(), s.getOneEnd().position().y(), s.getTheOtherEnd().position().x(), s.getTheOtherEnd().position().y());
	}
	
}
