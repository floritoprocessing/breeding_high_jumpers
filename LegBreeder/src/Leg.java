import florito.genetics.Animal;
import processing.core.PApplet;
import processing.core.PGraphics;
import traer.physics.Particle;
import traer.physics.ParticleSystem;
import traer.physics.Spring;


public class Leg extends Animal {
	
	//private final ParticleSystem physics;
	
	private ParticleSystem physics;
	
	// TO BREED:
	/**
	 * A, B, C, D, E, F, G
	 */
	public LegParticle A = new LegParticle(), 
	B = new LegParticle(), 
	C = new LegParticle(), 
	D = new LegParticle(), 
	E = new LegParticle(), 
	F = new LegParticle(), 
	G = new LegParticle();
	public RangedFloat rotationDistance = new RangedFloat();
	
	/**
	 * Rotation center is always 0/0/0
	 */
	private Particle rotationCenter;
	private float rotation = 0;
	
	/**
	 * Particles are deducted from the LegParticles
	 */
	private Particle PA, PB, PC, PD, PE, PF, PG;
	
	/**
	 * Springs are deducted from the LegParticles
	 */
	private Spring ab, bc, cd, bd, ce, df, ef, eg, fg, af;
	
	private Particle[] allParticles;
	private Spring[] allSprings;
	
	private static final float mass = 1.0f;
	private static final float strength = 1.0f;
	private static final float damping = 0.5f;

	public Leg(ParticleSystem physics, RangedFloat rotationDistance, LegParticle... cen_A_G) {
		this(physics);
		
		if (cen_A_G.length!=7) {
			throw new RuntimeException("Cannot create leg. Need exactly 7 LegParticles");
		}
		
		A = cen_A_G[0];
		B = cen_A_G[1];
		C = cen_A_G[2];
		D = cen_A_G[3];
		E = cen_A_G[4];
		F = cen_A_G[5];
		G = cen_A_G[6];
		this.rotationDistance = rotationDistance;
	
		makeStructure();
		
	}
	
	
	public Leg(ParticleSystem physics) {
		this.physics = physics;
		rotationCenter = physics.makeParticle(mass, 0, 0, 0);
	}
		
	
	
	@Override
	public void insertDNA(byte[] byteArray) {
		super.insertDNA(byteArray);
		makeStructure();
	}


	private void makeStructure() {
		
		PA = physics.makeParticle(mass, A.x(), A.y(), 0);//cen_A_G[1];
		PB = physics.makeParticle(mass, B.x(), B.y(), 0);//cen_A_G[2];
		PC = physics.makeParticle(mass, C.x(), C.y(), 0);//cen_A_G[3];
		PD = physics.makeParticle(mass, D.x(), D.y(), 0);//cen_A_G[4];
		PE = physics.makeParticle(mass, E.x(), E.y(), 0);//cen_A_G[5];
		PF = physics.makeParticle(mass, F.x(), F.y(), 0);//cen_A_G[6];
		PG = physics.makeParticle(mass, G.x(), G.y(), 0);//cen_A_G[7];
		
		rotate(PA, rotationCenter, rotationDistance.value, rotation);
		
		rotationCenter.makeFixed();
		PA.makeFixed();
		PD.makeFixed();
		
		ab = makeSpring(physics, PA, PB, strength, damping);
		
		bc = makeSpring(physics, PB, PC, strength, damping);
		cd = makeSpring(physics, PC, PD, strength, damping);
		bd = makeSpring(physics, PB, PD, strength, damping);
		
		ce = makeSpring(physics, PC, PE, strength, damping);
		df = makeSpring(physics, PD, PF, strength, damping);
		ef = makeSpring(physics, PE, PF, strength, damping);
		
		eg = makeSpring(physics, PE, PG, strength, damping);
		fg = makeSpring(physics, PF, PG, strength, damping);
		
		af = makeSpring(physics, PA, PF, strength, damping);
		
		allParticles = new Particle[] {
				rotationCenter,
				PA, PB, PC, PD, PE, PF, PG
		};
		allSprings = new Spring[] {
				ab, af,
				bc, bd,
				cd, ce,
				df,
				ef, eg,
				fg
		};
		
		//getBreedableVars();
	}
	
	private Spring makeSpring(ParticleSystem physics, Particle a, Particle b, float strength, float damping) {
		float dx = a.position().x()-b.position().x();
		float dy = a.position().y()-b.position().y();
		float restlength = (float)(Math.sqrt(dx*dx+dy*dy));
		return physics.makeSpring(a, b, strength, damping, restlength);
	}
	
	private static void rotate(Particle a, Particle center, float distance, float radians) {
		float x = (float)(distance*Math.cos(radians));
		float y = (float)(distance*Math.sin(radians));
		a.position().setX(center.position().x() + x);
		a.position().setY(center.position().y() + y);
	}
	
	public Spring[] getAllSprings() {
		return allSprings;
	}
	
	public Particle[] getAllParticles() {
		return allParticles;
	}
	
	public void drawLeg(PApplet pa, float offsetX, float offsetY, float xScale, float yScale, PGraphics trace) {
		
		pa.pushMatrix();
		pa.translate(offsetX, offsetY);
		pa.scale(xScale, yScale, 0);
		pa.ellipseMode(PApplet.RADIUS);
		pa.stroke(0,0,255);
		pa.ellipse(rotationCenter.position().x(), rotationCenter.position().y(), rotationDistance.value, rotationDistance.value);
		
		pa.stroke(255,0,0);
		for (Particle p:allParticles) {
			if (p.isFixed()) {
				pa.fill(255,0,0);
			} else {
				pa.noFill();
			}
			pa.ellipse(p.position().x(), p.position().y(), 5, 5);
		}
		
		pa.stroke(0,0,0);
		for (Spring s:allSprings) {
			pa.line(s.getOneEnd().position().x(), s.getOneEnd().position().y(),
					s.getTheOtherEnd().position().x(), s.getTheOtherEnd().position().y());
		}
		
		pa.popMatrix();
		
		trace.beginDraw();
		trace.stroke(0);
		trace.fill(0);
		trace.translate(offsetX, offsetY);
		trace.scale(xScale, yScale);
		trace.point(PG.position().x(), PG.position().y());
		trace.endDraw();
		
		
				
		
	}
	
	public void moveLeg() {
		rotation -= 0.1f;
		rotate(PA, rotationCenter, rotationDistance.value, rotation);
	}

	public void removeFromParticleSystem(ParticleSystem physics) {
		for (Particle p:allParticles) {
			physics.removeParticle(p);
		}
		for (Spring s:allSprings) {
			physics.removeSpring(s);
		}
		
	}


	@Override
	public int size() {
		return 0;
	}


	@Override
	public void createStructureFromDNA() {
		// TODO Auto-generated method stub
		
	}
	
	
}
