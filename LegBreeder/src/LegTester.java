import florito.genetics.Genetics;
import processing.core.PApplet;
import processing.core.PGraphics;
import traer.physics.ParticleSystem;


public class LegTester extends PApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8673124429407479452L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"LegTester"});
	}
	
	PGraphics trace;
	
	ParticleSystem physics = new ParticleSystem(0, 0.1f);
	final float mass = 1;
	
	final int nrX = 5;
	final int nrY = 5;
	Leg[] leg = new Leg[nrX*nrY];
	
	
	
	public void setup() {
		size(800, 800,P3D);
		smooth();
		
		//leg = new Leg();
		
		Leg oneLeg = createStandardleg();
		//oneLeg.A.setBreedable(false);
		oneLeg.defineBreedableVars();
		
		
		for (int i=0;i<leg.length;i++) {
			//leg[i] = createLegVariation(oneLeg, 50);
			//leg[i] = new Leg();
			leg[i] = new Leg(physics);
			Genetics.breed(leg[i], 0.2f, new Leg[] {oneLeg});
		}
		
		oneLeg.removeFromParticleSystem(physics);
		//createParticlesAndDistances(false);
		
		//makeSprings();
				
		trace = createGraphics(width, height, P3D);
		trace.beginDraw();
		trace.background(255);
		trace.endDraw();
		
		
		
	}

	
	private Leg createStandardleg() {
		RangedFloat rotationDistance = new RangedFloat(25);
		//LegParticle rotationCenter = new LegParticle(0, 0);
		LegParticle A = new LegParticle(0, 0);
		
		int xOff = 0;
		int yOff = 0;
		LegParticle B = new LegParticle(-50+xOff, -50+yOff);
		LegParticle C = new LegParticle(-100+xOff, -40+yOff);
		LegParticle D = new LegParticle(-75+xOff, 0+yOff);
		LegParticle E = new LegParticle(-100+xOff, 25+yOff);
		LegParticle F = new LegParticle(-50+xOff, 50+yOff);
		LegParticle G = new LegParticle(-100+xOff, 100+yOff);
		
		return new Leg(
				physics,
				rotationDistance
				,new LegParticle[] {
				A, B, C, D, E, F, G
		});
	}
	

	private Leg createLegVariation(Leg baseLeg, float positionVariance) {
		if (baseLeg==null) {
			
			return createStandardleg();
			
		} else {
			
			RangedFloat rotationDistance = new RangedFloat(Math.max(5, baseLeg.rotationDistance.value + gauss(positionVariance)));
			
			//LegParticle rotationCenter = new LegParticle(0, 0);
			float[] p = positionVariance(baseLeg.A, positionVariance);
			LegParticle newA = new LegParticle(p[0],p[1]);
			
			float xOff = gauss(positionVariance);
			float yOff = gauss(positionVariance);
			p = positionVariance(baseLeg.B, positionVariance);
			LegParticle newB = new LegParticle(p[0]+xOff, p[1]+yOff);
			p = positionVariance(baseLeg.C, positionVariance);
			LegParticle newC = new LegParticle(p[0]+xOff, p[1]+yOff);
			p = positionVariance(baseLeg.D, positionVariance);
			LegParticle newD = new LegParticle(p[0]+xOff, p[1]+yOff);
			p = positionVariance(baseLeg.E, positionVariance);
			LegParticle newE = new LegParticle(p[0]+xOff, p[1]+yOff);
			p = positionVariance(baseLeg.F, positionVariance);
			LegParticle newF = new LegParticle(p[0]+xOff, p[1]+yOff);
			p = positionVariance(baseLeg.G, positionVariance);
			LegParticle newG = new LegParticle(p[0]+xOff, p[1]+yOff);
			
			return new Leg(
					physics,
					rotationDistance,
					new LegParticle[] {
							newA, newB, newC, newD, newE, newF, newG
					});

		}
		
	}
	
	private static float[] positionVariance(LegParticle p, float range) {
		return new float[] {
			p.x() + gauss(range),
			p.y() + gauss(range)
		};
	}

	private static float gauss(float plusMinusValue) {
		float gauss = (float)((Math.random()+Math.random()+Math.random()+Math.random()+Math.random()+Math.random()-3.0)/3.0);
		return gauss*plusMinusValue;
	}
	
	

	
	
	public void mousePressed() {
		
		// clear trace
		trace.beginDraw();
		trace.noStroke();
		trace.fill(255,128);
		trace.rect(0,0,width,height);
		trace.endDraw();
		
		// create a new Leg based on the old one:
		//Leg newLeg = createParticlesAndDistances(leg, 1);
		
		// remove all of leg from the particle system
		//leg.removeFromParticleSystem();
		
		//leg = newLeg;
	}
	
	public void draw() {
		image(trace,0,0);
		
		int i=0;
		for (int y=0;y<nrY;y++) {
			float offsetY = (float)((y+0.5)/nrY*width);
			for (int x=0;x<nrX;x++) {
				float offsetX = (float)((x+0.5)/nrX*height);
				float xScale = 2.0f/nrX * 1.5f;
				float yScale = 2.0f/nrY * 1.5f;
				float scale = Math.min(xScale, yScale);
				leg[i].drawLeg(this, offsetX, offsetY, scale, scale, trace);
				leg[i].moveLeg();
				i++;
			}
		}
		
		if (frameCount%25==0) {
			trace.beginDraw();
			trace.noStroke();
			trace.fill(255,32);
			trace.rect(0,0,width,height);
			trace.endDraw();
		}
		
		physics.tick();
	}
	
	

}
