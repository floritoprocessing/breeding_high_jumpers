import florito.genetics.Breedable;


public class LegParticle implements Breedable {

	private boolean breedable = true;
	private RangedFloat x, y;
	
	public LegParticle() {
		x = new RangedFloat();
		y = new RangedFloat();
	}
	
	public LegParticle(float x, float y) {
		this.x = new RangedFloat(x);
		this.y = new RangedFloat(y);
	}
	
	public float x() {
		return x.value;
	}
	
	public float y() {
		return y.value;
	}

	@Override
	public byte[] toByteArray() {
		
		byte[] xArray = x.toByteArray();
		byte[] yArray = y.toByteArray();
		byte[] out = new byte[xArray.length+yArray.length];
		System.arraycopy(xArray, 0, out, 0, xArray.length);
		System.arraycopy(yArray, 0, out, xArray.length, yArray.length);
		return out;
		
	}

	@Override
	public void fromByteArray(byte[] byteArray) {
		
		byte[] byteArrayX = new byte[4];
		byte[] byteArrayY = new byte[4];
		System.arraycopy(byteArray, 0, byteArrayX, 0, 4);
		System.arraycopy(byteArray, 4, byteArrayY, 0, 4);
		x.fromByteArray(byteArrayX);
		y.fromByteArray(byteArrayY);
		
	}

	@Override
	public boolean isBreedable() {
		return breedable;
	}

	@Override
	public void setBreedable(boolean breedable) {
		this.breedable = breedable;
	}

	@Override
	public String toString() {
		return x+", "+y;
	}
}
