import florito.genetics.Breedable;
import florito.genetics.FloatByteArrayUtil;


public class RangedFloat implements Breedable {

	private boolean breedable = true;
	
	public static float min = -150;
	public static float max = 150;
	public float value=0;
	
	public RangedFloat() {
		
	}
	
	public RangedFloat(float value) {
		if (value<min || value>max) {
			throw new RuntimeException("Value out of range!");
		}
		this.value = value;
	}
	
	public void random() {
		value = (float)(min+Math.random()*max-min);
	}

	@Override
	public void fromByteArray(byte[] byteArray) {
		float rangeToLargest = FloatByteArrayUtil.byteArrayToFloat(byteArray); // 0..largest value
		float rangePercentage = rangeToLargest/Integer.MAX_VALUE;
		System.out.println(rangePercentage);
		value = min + rangePercentage*(max-min);
	}

	@Override
	public boolean isBreedable() {
		return breedable;
	}

	@Override
	public void setBreedable(boolean breedable) {
		this.breedable = breedable;
	}

	@Override
	public byte[] toByteArray() {
		float rangePercentage = (value-min)/(max-min); // 0..1
		//System.out.println(rangePercentage);
		float rangeToLargest = rangePercentage*Integer.MAX_VALUE; // 0..largest value
		return FloatByteArrayUtil.floatToByteArray(rangeToLargest);
	}
	
	@Override
	public String toString() {
		return value+" ("+min+".."+max+")";
	}
	
}
