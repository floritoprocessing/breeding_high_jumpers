import florito.genetics.Breedable;

/**
 * LegSize: 1..256
 * @author Marcus
 *
 */
public class Length implements Breedable {

	private int size = 1;
	
	public Length() {
	}
	
	public int getSize() {
		return size;
	}
	
	@Override
	public void fromByteArray(byte[] byteArray) {
		size = (int)byteArray[0];
		if (size<0) {
			size +=256;
		}
		size += 10;
	}
	
	@Override
	public byte[] toByteArray() {
		return new byte[] {(byte)(size-10)};
	}

	@Override
	public boolean isBreedable() {
		return true;
	}

	@Override
	public void setBreedable(boolean breedable) {
	}

	

}
