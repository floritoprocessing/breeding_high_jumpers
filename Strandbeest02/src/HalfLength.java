import florito.genetics.Breedable;

/**
 * LegSize: 1..128
 * @author Marcus
 *
 */
public class HalfLength implements Breedable {

	private int size = 1;
	
	public HalfLength() {
	}
	
	public int getSize() {
		return size;
	}
	
	@Override
	public void fromByteArray(byte[] byteArray) {
		size = (int)byteArray[0];
		if (size<0) {
			size +=256;
		}
		size /= 2;
		size += 10;
	}
	
	@Override
	public byte[] toByteArray() {
		int v = (size-10)*2;
		return new byte[] {(byte)v};
	}

	@Override
	public boolean isBreedable() {
		return true;
	}

	@Override
	public void setBreedable(boolean breedable) {
	}

	

}
