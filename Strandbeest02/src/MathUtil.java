import traer.physics.Particle;
import traer.physics.Vector3D;


public class MathUtil {
	
	public static Vector3D[] intersectionTwoCircles(Particle P0, Particle P1, float r0, float r1) {
		return intersectionTwoCircles(P0.position(), P1.position(), r0, r1);
	}
	
	public static Vector3D[] intersectionTwoCircles(Vector3D P0, Vector3D P1, float r0, float r1) {
		float d = P1.distanceTo(P0);
		if (d>r0+r1) {
			return null; // no solution: the circles are separate
		}
		else if (d<Math.abs(r0-r1)) {
			return null; // no solution: one circle is contained within the other
		}
		else if (d==0 && r0==r1) {
			return null; // circles are coincident -> infinite number of solutions
		}
		else {
			//http://local.wasp.uwa.edu.au/~pbourke/geometry/2circle/
			float x0 = P0.x();
			float y0 = P0.y();
			float x1 = P1.x();
			float y1 = P1.y();
			
			float a = (r0*r0-r1*r1+d*d)/(2*d);
			
			float h = (float)Math.sqrt(r0*r0-a*a);
			
			Vector3D P1minP0 = new Vector3D(P1);
			P1minP0.subtract(P0);
			Vector3D P1minP0MulADivD = new Vector3D(P1minP0);
			P1minP0MulADivD.multiplyBy(a/d);
			
			Vector3D P2 = new Vector3D(P0);
			P2.add(P1minP0MulADivD);
			
			float x2 = P2.x();
			float y2 = P2.y();
			
			float x3_0 = x2 + h * (y1-y0)/d;
			float x3_1 = x2 - h * (y1-y0)/d;
			float y3_0 = y2 + h * (x1-x0)/d;
			float y3_1 = y2 - h * (x1-x0)/d;
			
			if (x3_0==x3_1 && y3_0==y3_1) {
				return new Vector3D[] { new Vector3D(x3_0,y3_0,0) };
			} else {
				return new Vector3D[] { new Vector3D(x3_0,y3_0,0), new Vector3D(x3_1,y3_1,0) };
			}
		}
	}

}
