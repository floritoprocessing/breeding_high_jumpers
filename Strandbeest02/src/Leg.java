import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import processing.core.PApplet;
import traer.physics.Particle;
import traer.physics.ParticleSystem;
import traer.physics.Spring;
import traer.physics.Vector3D;
import florito.genetics.Animal;
import florito.genetics.Breedable;




/*
 *        **A
 *       ** **
 *      * *   ** 
 *     *  *     **
 *    *   *       **
 *   *    *         **
 * B****  *       P  **R
 *  *   ***Q        **
 *  *     *       **
 *  *    *      **
 *  *    *    **
 *  *    *D **
 * C********
 *  *    *
 *   *  *
 *   *  *
 *    **
 *    **
 *    E
 */ 

public class Leg extends Animal {

	private final ParticleSystem physics;
	
	public HalfLength PtoR = new HalfLength();
	public HalfLength PtoQ = new HalfLength();
	
	public Length AtoR = new Length();
	public Length AtoQ = new Length();
	
	public Length BtoA = new Length();
	public Length BtoQ = new Length();
	
	public Length BtoC = new Length();
	public Length QtoD = new Length();
	public Length CtoD = new Length();
	public Length DtoR = new Length();
	
	public Length CtoE = new Length();
	public Length DtoE = new Length();
	
	/**
	 * Pivot
	 */
	private Particle P;
	/**
	 * Rotate around pivot
	 */
	private Particle R;
	/**
	 * Other fixed axis
	 */
	private Particle Q;
	
	private Particle A, B, C, D, E;
	
	
	private Particle[] allParticles = new Particle[0];
	private Spring[] allSprings = new Spring[0];
	
	private float pivotRotation = 0f;
	private boolean springIntersected = false;
	
	private static final float mass = 0.1f;
	private static final float strength = 0.1f;
	private static final float damping = 0.01f;
	
	public Leg(ParticleSystem physics) {
		this.physics = physics;
		defineBreedableVars();
	}
	
	public void randomize() {
		
		/*
		 * Randomize all breedable vars
		 */
		for (Breedable b:allBreedableVars) {
			byte[] byteArray = b.toByteArray();
			for (int i=0;i<byteArray.length;i++) {
				byteArray[i] = (byte)(Math.random()*256);
			}
			b.fromByteArray(byteArray);
		}
			
		boolean physicsOk = false;
		
		while (!physicsOk) {
			
			for (Particle p:allParticles) {
				physics.removeParticle(p);
			}
			for (Spring s:allSprings) {
				physics.removeSpring(s);
			}
			allParticles = new Particle[0];
			allSprings = new Spring[0];
			
			physicsOk = createPhysics();
		}
		
	}
	
	/**
	 * Returns false if the physics failed creating (due to wrong distances)
	 * @return
	 */
	private boolean createPhysics() {
		
		ArrayList<Particle> registeredParticles = new ArrayList<Particle>();
		ArrayList<Spring> registeredSprings = new ArrayList<Spring>();
		
		/*
		 * Create pivot P on 0/0/0
		 */
		P = physics.makeParticle(mass, 0, 0, 0);
		P.makeFixed();
		registeredParticles.add(P);
		
		/*
		 * Create rotator R (with displayable spring)
		 */
		R = physics.makeParticle(mass, PtoR.getSize(), 0, 0);
		R.makeFixed();
		registeredParticles.add(R);
		registeredSprings.add(physics.makeSpring(P, R, strength, damping, PtoR.getSize()));
		
		/*
		 * Create fixed point Q
		 */
		Q = physics.makeParticle(mass, -PtoR.getSize()-PtoQ.getSize(), 0, 0);
		Q.makeFixed();
		registeredParticles.add(Q);
		
		/*
		 * Make A above Q and R
		 */
		float r0 = AtoQ.getSize();
		float r1 = AtoR.getSize();
		Vector3D[] twoPoints = null;//
		while (twoPoints==null) {
			twoPoints = MathUtil.intersectionTwoCircles(Q, R, r0, r1);
			r0*=1.5f;
			r1*=1.5f;
		}
		Vector3D foundPoint = twoPoints[0];
		if (twoPoints.length==2) {
			if (twoPoints[1].y()<twoPoints[0].y()) {
				foundPoint = twoPoints[1];
			}
		}
		A = physics.makeParticle(mass, foundPoint.x(), foundPoint.y(), 0);
		registeredParticles.add(A);
		registeredSprings.add(physics.makeSpring(A, Q, strength, damping, r0));
		registeredSprings.add(physics.makeSpring(A, R, strength, damping, r1));
		
		/*
		 * Make B, so that it's furthest away from R
		 */
		
		r0 = BtoA.getSize();
		r1 = BtoQ.getSize();
		twoPoints = null;//
		while (twoPoints==null) {
			twoPoints = MathUtil.intersectionTwoCircles(A, Q, r0, r1);
			r0*=1.5f;
			r1*=1.5f;
		}
		foundPoint = twoPoints[0];
		if (twoPoints.length==2) {
			float distance0 = twoPoints[0].distanceSquaredTo(R.position());
			float distance1 = twoPoints[1].distanceSquaredTo(R.position());
			if (distance1>distance0) {
				foundPoint = twoPoints[1];
			}
		}
		B = physics.makeParticle(mass, foundPoint.x(), foundPoint.y(), 0);
		registeredParticles.add(B);
		registeredSprings.add(physics.makeSpring(B, A, strength, damping, r0));
		registeredSprings.add(physics.makeSpring(B, Q, strength, damping, r1));
		
		/*
		 * Make C and D
		 */
		float x = B.position().x();
		float y = B.position().y() + BtoC.getSize();
		C = physics.makeParticle(mass, x, y, 0);
		registeredParticles.add(C);
		registeredSprings.add(physics.makeSpring(B, C, strength, damping, BtoC.getSize()));
		
		x = Q.position().x();
		y = Q.position().y() + QtoD.getSize();
		D = physics.makeParticle(mass, x, y, 0);
		registeredParticles.add(D);
		registeredSprings.add(physics.makeSpring(Q, D, strength, damping, QtoD.getSize()));
		
		registeredSprings.add(physics.makeSpring(C, D, strength, damping, CtoD.getSize()));
		registeredSprings.add(physics.makeSpring(D, R, strength, damping, DtoR.getSize()));
		
		
		/*
		 * Make E below C and D
		 */
		r0 = CtoE.getSize();
		r1 = DtoE.getSize();
		twoPoints = null;//
		while (twoPoints==null) {
			twoPoints = MathUtil.intersectionTwoCircles(C, D, r0, r1);
			r0*=1.5f;
			r1*=1.5f;
		}
		foundPoint = twoPoints[0];
		if (twoPoints.length==2) {
			if (twoPoints[1].y()>twoPoints[0].y()) {
				foundPoint = twoPoints[1];
			}
		}
		E = physics.makeParticle(mass, foundPoint.x(), foundPoint.y(), 0);
		registeredParticles.add(E);
		registeredSprings.add(physics.makeSpring(C, E, strength, damping, r0));
		registeredSprings.add(physics.makeSpring(D, E, strength, damping, r1));
		
		
		allParticles = registeredParticles.toArray(new Particle[0]);
		allSprings = registeredSprings.toArray(new Spring[0]);
		
		for (Spring s:allSprings) {
			if (Float.isNaN(s.currentLength())) {
				//return false;
			}
		}
		
		return true;
	}
	
	public void draw(PApplet pa) {
		
		pa.noStroke();
		pa.ellipseMode(PApplet.RADIUS);
		for (Particle p:allParticles) {
			if (p.isFixed()) {
				pa.fill(128,0,0);
			} else {
				pa.fill(0);
			}
			pa.ellipse(p.position().x(), p.position().y(), 20, 20);
		}
		
		pa.stroke(0,0,0);
		for (Spring s:allSprings) {
			pa.line(s.getOneEnd().position().x(), s.getOneEnd().position().y(), s.getTheOtherEnd().position().x(), s.getTheOtherEnd().position().y());
		}
		
	}
	
	public Spring[] getSprings() {
		return allSprings;
	}
	
	public void rotate(float rad) {
		if (!springIntersected) {
			pivotRotation -= rad;
			float x = (float)(Math.cos(pivotRotation)*PtoR.getSize());
			float y = (float)(Math.sin(pivotRotation)*PtoR.getSize());
			R.position().set(x,y,0);
		}
	}
	
	public void checkForSpringIntersection() {
		if (!springIntersected) {
			int len = allSprings.length;
			for (int i=0;i<len-1;i++) {
				for (int j=i+1;j<len;j++) {
					//System.out.println("test "+i+" adn "+j);
					if (innerIntersection(allSprings[i],allSprings[j])) {
						springIntersected = true;
						return;
					}
				}
			}
		}
	}
	
	private static boolean innerIntersection(Spring s1, Spring s2) {
		
		//System.out.println("Testing intersection on "+legID);
		
		float s1x0 = s1.getOneEnd().position().x();
		float s1y0 = s1.getOneEnd().position().y();
		float s1x1 = s1.getTheOtherEnd().position().x();
		float s1y1 = s1.getTheOtherEnd().position().y();
		
		float s2x0 = s2.getOneEnd().position().x();
		float s2y0 = s2.getOneEnd().position().y();
		float s2x1 = s2.getTheOtherEnd().position().x();
		float s2y1 = s2.getTheOtherEnd().position().y();
		
		float s1dx = s1x1-s1x0;
		float s1dy = s1y1-s1y0;
		float s2dx = s2x1-s2x0;
		float s2dy = s2y1-s2y0;
		
		// short so that there are no point overlaps
		s1dx *= 0.99f;
		s1dy *= 0.99f;
		s2dx *= 0.99f;
		s2dy *= 0.99f;
		
		Point2D.Float P0 = new Point2D.Float(s1x0,s1y0);
		Point2D.Float P1 = new Point2D.Float(s1x0+s1dx,s1y0+s1dy);
		Point2D.Float P2 = new Point2D.Float(s2x0,s2y0);
		Point2D.Float P3 = new Point2D.Float(s2x0+s2dx,s2y0+s2dy);
		
		Line2D.Float line0 = new Line2D.Float(P0, P1);
		Line2D.Float line1 = new Line2D.Float(P2, P3);
		
		return line0.intersectsLine(line1);
	}
	
	private static void particleCircle(PApplet pa, Particle p, float radius) {
		pa.ellipse(p.position().x(), p.position().y(), radius, radius);
	}
	
	private static void particleLine(PApplet pa, Particle a, Particle b) {
		pa.line(a.position().x(), a.position().y(), b.position().x(), b.position().y());
	}
	
	private static void springLine(PApplet pa, Spring s) {
		pa.line(s.getOneEnd().position().x(), s.getOneEnd().position().y(), s.getTheOtherEnd().position().x(), s.getTheOtherEnd().position().y());
	}
	
}
