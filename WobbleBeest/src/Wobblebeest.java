import java.util.ArrayList;

import processing.core.PApplet;
import traer.physics.Particle;
import traer.physics.ParticleSystem;
import traer.physics.Spring;
import traer.physics.Vector3D;
import breedvars.SpringBehaviour;
import florito.genetics.Animal;


public class Wobblebeest extends Animal {

	private static final String NL = System.getProperty("line.separator");
	private final ParticleSystem physics;
	
	private Vector3D[] lastVelocity;
	
	public SpringBehaviour ab = new SpringBehaviour();
	public SpringBehaviour bc = new SpringBehaviour();
	public SpringBehaviour cd = new SpringBehaviour();
	public SpringBehaviour da = new SpringBehaviour();
	public SpringBehaviour ac = new SpringBehaviour();
	public SpringBehaviour bd = new SpringBehaviour();
	
	private Spring AB, BC, CD, DA;
	private Spring AC, BD;
	
	private ArrayList<Particle> onFloor = new ArrayList<Particle>();
	
	private ArrayList<Particle> allParticles = new ArrayList<Particle>();
	private ArrayList<Spring> allSprings = new ArrayList<Spring>();
	
	private static float mass = 0.1f;
	private static float strength = 0.16f;
	private static float damping = 0.01f;
	
	private boolean alive = true;
	
	public Wobblebeest(ParticleSystem physics) {
		this.physics = physics;
		//defineBreedableVars();
	}
	
	
	public int size() {
		return 6*ab.size();
	}
	
	
	public void createStructureFromDNA() {
		/*
		 * Remove Springs and Particles from physics
		 * Clear register
		 */
		for (Spring s:allSprings) {
			physics.removeSpring(s);
		}
		allSprings.clear();
		
		for (Particle p:allParticles) {
			physics.removeParticle(p);
		}
		allParticles.clear();
		
		/*
		 * Create AB
		 */
		Particle A = physics.makeParticle(mass, 0, 0, 0);
		allParticles.add(A);
		
		float len = ab.calculateTemporalLength();
		Particle B = physics.makeParticle(mass, len, 0, 0);
		allParticles.add(B);
		
		AB = physics.makeSpring(A, B, strength, damping, len);
		allSprings.add(AB);
		
		/*
		 * Create BC
		 */
		len = bc.calculateTemporalLength();
		Particle C = physics.makeParticle(mass, B.position().x(), -len, 0);
		allParticles.add(C);
		
		BC = physics.makeSpring(B, C, strength, damping, len);
		allSprings.add(BC);
		
		/*
		 * Create DA
		 */
		len = da.calculateTemporalLength();
		Particle D = physics.makeParticle(mass, 0, -len, 0);
		allParticles.add(D);
		
		DA = physics.makeSpring(D, A, strength, damping, len);
		allSprings.add(DA);
		
		/*
		 * Create CD
		 */
		CD = physics.makeSpring(C, D, strength, damping, cd.calculateTemporalLength());
		allSprings.add(CD);
		
		
		/*
		 * Diagonals
		 */
		AC = physics.makeSpring(A, C, strength, damping, ac.calculateTemporalLength());
		allSprings.add(AC);
		BD = physics.makeSpring(B, D, strength, damping, bd.calculateTemporalLength());
		allSprings.add(BD);
		
		for (Particle p:allParticles) {
			p.position().add(0,-100,0);
		}
	}
	
	public void tick(float fac) {
		
		if (alive) {
			
			for (int i=0;i<onFloor.size();i++) {
				onFloor.get(i).makeFree();
				onFloor.get(i).velocity().set(lastVelocity[i]);
			}
			onFloor.clear();
			
			ab.tick(fac);
			AB.setRestLength(ab.calculateTemporalLength());
			
			bc.tick(fac);
			BC.setRestLength(bc.calculateTemporalLength());
			
			cd.tick(fac);
			CD.setRestLength(cd.calculateTemporalLength());
			
			da.tick(fac);
			DA.setRestLength(da.calculateTemporalLength());
			
			/*
			 * Diagonals
			 */
			ac.tick(fac);
			AC.setRestLength(ac.calculateTemporalLength());
			bd.tick(fac);
			BD.setRestLength(bd.calculateTemporalLength());
			
			
			/*
			 * Check floor
			 */
			
			ArrayList<Vector3D> lastVels = new ArrayList<Vector3D>();
			
			for (Particle p:allParticles) {
				if (p.position().y()>0 && p.velocity().y()>0) {
					p.position().setY(0);
					p.velocity().setY(-p.velocity().y());
					lastVels.add(new Vector3D(p.velocity()));
					p.makeFixed();
					onFloor.add(p);
				}
			}
			
			lastVelocity = lastVels.toArray(new Vector3D[0]);
		
		}
	}
	
	
	public void draw(PApplet pa, float left, float right) {
		pa.translate(-(left+right)/2.0f, 0);
		
		if (alive) {
			pa.stroke(0);
			pa.strokeWeight(1.0f);
		} else {
			pa.stroke(255,0,0);
			pa.strokeWeight(3.0f);
		}
		pa.line(left, 0, right, 0);
		
		pa.strokeWeight(1.0f);
		pa.noStroke();
		pa.fill(0);
		pa.ellipseMode(PApplet.RADIUS);
		for (Particle p:allParticles) {
			pa.ellipse(p.position().x(),p.position().y(),10,10);
		}
		
		pa.noFill();
		pa.stroke(0);
		for (Spring s:allSprings) {
			pa.line(s.getOneEnd().position().x(), s.getOneEnd().position().y(), s.getTheOtherEnd().position().x(), s.getTheOtherEnd().position().y());
		}
		
		
		
		
	}
	
	@Override
	public String toString() {
		return ab+NL+bc+NL+cd+NL+da+NL+ac+NL+bd;
	}

	/**
	 * Returns true if stopped on this call
	 * @param xPos
	 * @return
	 */
	public boolean stopIfOutside(float xPos) {
		if (alive) {
			float averageX = 0;
			for (Particle p:allParticles) {
				averageX += p.position().x();
			}
			averageX /= allParticles.size();
			
			if (averageX>xPos || averageX<-xPos) {
				alive = false;
				
				for (Particle p:allParticles) {
					p.makeFixed();
				}
			}
			
			if (!alive) {
				return true;
			}
		}
		
		return false;
	}
	
	public void removeFromPhysics() {
		for (Spring s:allSprings) {
			physics.removeSpring(s);
		}
		allSprings.clear();
		for (Particle p:allParticles) {
			physics.removeParticle(p);
		}
		allParticles.clear();
	}
	
}
