import java.util.ArrayList;

import processing.core.PApplet;
import traer.physics.ParticleSystem;
import florito.genetics.Genetics;


public class WobbleBeestTester extends PApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2314755680147457497L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"WobbleBeestTester"});
	}
	
	int w=10;
	int h=20;
	Wobblebeest[] generation = new Wobblebeest[w*h];
	
	private final ParticleSystem physics = new ParticleSystem(0.1f,0.01f);
	
	int winnersForNextGeneration = 10;
	int randomForNextGeneration = 1;
	
	ArrayList<Wobblebeest> winners = new ArrayList<Wobblebeest>();
	
	int days = 0;
	
	public void settings() {
		size(800,800,P3D);
	}
	
	public void setup() {
		
			
		/*for (int i=0;i<100;i++) {
			
			byte[] rndByteArray = new byte[4];
			for (int b=0;b<rndByteArray.length;b++) {
				rndByteArray[b] = (byte)(Math.random()*256);
				//System.out.print(rndByteArray[b]+", ");
			}
			// switch byte 0 to 0x7F or 0xFF to increase chance on NaN and Infinity
			//rndByteArray[0] = (byte)(0xFF);
			//rndByteArray = GeneticUtils.intToByteArray(0xFFC00001);
			
			
			System.out.print("random DNA: "+GeneticUtils.byteArrayToBitString(rndByteArray));
			Primitive.Float prim0 = new Primitive.Float();
			Primitive.Float prim1 = new Primitive.Float();
			
			prim0.fromDNAByteArray(rndByteArray);
			
			byte[] byteArray = prim0.toDNAByteArray();
			System.out.print("\t -> primitive value: "+prim0.getValue()+"\t-> DNA: "+GeneticUtils.byteArrayToBitString(byteArray)+" -> ");
			
			prim1.fromDNAByteArray(byteArray);
			System.out.print("\t new primitive from DNA: "+prim1.getValue());
			
			if (prim0.getValue()==prim1.getValue()) {
				System.out.println(" OK! ");
			} else {
				System.out.println(" ERROR ON DIFFERENCE ");
			}
			
			//System.out.println("\t -> "+GeneticUtils.byteArrayToBitString(prim1.toDNAByteArray()));
		}*/
	
		
		for (int i=0;i<generation.length;i++) {
			generation[i] = new Wobblebeest(physics);
			Genetics.randomize(generation[i]);
			//generation[i].randomizeDNA();
		}
		
	}
	
	public void draw() {
		background(240);
		
		int i=0;
		float largest = Math.max(w,h);
		for (int y=0;y<h;y++) {
			for (int x=0;x<w;x++) {
				
				pushMatrix();
				translate((x+0.5f)/w*width,(y+0.5f)/h*height);
				
				scale(1.2f/largest, 1.2f/largest);
				generation[i].draw(this,-500,500);
				popMatrix();
				
				i++;
			}
		}
		
		for (i=0;i<generation.length;i++) {
			generation[i].tick(5.0f);
		}
		physics.tick();
		
		for (i=0;i<generation.length;i++) {
			boolean stopped = generation[i].stopIfOutside(500);
			if (stopped) {
				//System.out.println("Animal "+i+" has won ("+(winners.size()+1)+"/"+winnersForNextGeneration+")");
				winners.add(generation[i]);
			}
		}
		
		days++;
		
		if (winners.size()>=winnersForNextGeneration) {
			createNewGeneration();
		}
	}
	
	private void createNewGeneration() {
		
		System.out.println("Creating new generation after "+days+" days.");
		days = 0;

		/*
		 * Add random creatures to winners
		 */
		if (randomForNextGeneration>0) {
			for (int i=0;i<randomForNextGeneration;i++) {
				int index = (int)(Math.random()*generation.length);
				winners.add(generation[index]);
			}
		}
		
		/*
		 * Remove old generation
		 */
		for (Wobblebeest beest:generation) {
			beest.removeFromPhysics();
		}
		
		
		/*
		 * Create new generation
		 */
		Wobblebeest[] nextGeneration = new Wobblebeest[w*h];
		for (int i=0;i<nextGeneration.length;i++) {			
			Wobblebeest child = new Wobblebeest(physics);
			Wobblebeest parent0 = winners.get((int)(Math.random()*winners.size()));
			Wobblebeest parent1 = parent0;
			while (parent1==parent0) {
				parent1 = winners.get((int)(Math.random()*winners.size()));
			}
			
			//System.out.println("------------");
			
			Genetics.breed(child, 0.01f, new Wobblebeest[] {parent0,parent1});
			//child.createPhysics();
			//System.out.println("parent0: "+Genetics.asBinaryString(parent0));
			//System.out.println("parent1: "+Genetics.asBinaryString(parent1));		
			//System.out.println("child:   "+Genetics.asBinaryString(child));
			
			nextGeneration[i] = child;
		}
		
		/*
		 * Replace old generation
		 */
		generation = nextGeneration;
		
		winners.clear();
		
	}
	
}
