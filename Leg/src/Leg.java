import florito.genetics.Animal;
import processing.core.PApplet;
import processing.core.PGraphics;
import traer.physics.Particle;
import traer.physics.ParticleSystem;
import traer.physics.Spring;


public class Leg extends Animal {
	
	//private final ParticleSystem physics;
	
	// TO BREED:
	public Particle A, B, C, D, E, F, G;
	public float rotationDistance;
	
	/*
	 * Rotation center always 0/0/0
	 */
	private final Particle rotationCenter;
	private float rotation = 0;

	/**
	 * Springs are deducted from the Particles
	 */
	private Spring ab, bc, cd, bd, ce, df, ef, eg, fg, af;
	
	private Particle[] allParticles;
	private Spring[] allSprings;
	
	private static final float strength = 1.0f;
	private static final float damping = 0.5f;

	public Leg(ParticleSystem physics, float rotationDistance, Particle... cen_A_G) {
		if (cen_A_G.length!=8) {
			throw new RuntimeException("Cannot create leg. Need exactly 8 particles");
		}
		
		//this.physics = physics;
		this.rotationDistance = rotationDistance; 
		rotationCenter = cen_A_G[0];
		A = cen_A_G[1];
		B = cen_A_G[2];
		C = cen_A_G[3];
		D = cen_A_G[4];
		E = cen_A_G[5];
		F = cen_A_G[6];
		G = cen_A_G[7];
		
		rotate(A, rotationCenter, rotationDistance, rotation);
		
		rotationCenter.makeFixed();
		A.makeFixed();
		D.makeFixed();
		
		ab = makeSpring(physics, A, B, strength, damping);
		
		bc = makeSpring(physics, B, C, strength, damping);
		cd = makeSpring(physics, C, D, strength, damping);
		bd = makeSpring(physics, B, D, strength, damping);
		
		ce = makeSpring(physics, C, E, strength, damping);
		df = makeSpring(physics, D, F, strength, damping);
		ef = makeSpring(physics, E, F, strength, damping);
		
		eg = makeSpring(physics, E, G, strength, damping);
		fg = makeSpring(physics, F, G, strength, damping);
		
		af = makeSpring(physics, A, F, strength, damping);
		
		allParticles = new Particle[] {
				rotationCenter,
				A, B, C, D, E, F, G
		};
		allSprings = new Spring[] {
				ab, af,
				bc, bd,
				cd, ce,
				df,
				ef, eg,
				fg
		};
		
		//getBreedableVars();
	}
	
	private Spring makeSpring(ParticleSystem physics, Particle a, Particle b, float strength, float damping) {
		float dx = a.position().x()-b.position().x();
		float dy = a.position().y()-b.position().y();
		float restlength = (float)(Math.sqrt(dx*dx+dy*dy));
		return physics.makeSpring(a, b, strength, damping, restlength);
	}
	
	private static void rotate(Particle a, Particle center, float distance, float radians) {
		float x = (float)(distance*Math.cos(radians));
		float y = (float)(distance*Math.sin(radians));
		a.position().setX(center.position().x() + x);
		a.position().setY(center.position().y() + y);
	}
	
	public Spring[] getAllSprings() {
		return allSprings;
	}
	
	public Particle[] getAllParticles() {
		return allParticles;
	}
	
	public void drawLeg(PApplet pa, float offsetX, float offsetY, float xScale, float yScale, PGraphics trace) {
		
		pa.pushMatrix();
		pa.translate(offsetX, offsetY);
		pa.scale(xScale, yScale, 0);
		pa.ellipseMode(PApplet.RADIUS);
		pa.stroke(0,0,255);
		pa.noFill();
		pa.ellipse(rotationCenter.position().x(), rotationCenter.position().y(), rotationDistance, rotationDistance);
		
		pa.stroke(255,0,0);
		for (Particle p:allParticles) {
			if (p.isFixed()) {
				pa.fill(255,0,0);
			} else {
				pa.fill(0);
			}
			//pa.noFill();
			pa.ellipse(p.position().x(), p.position().y(), 5, 5);
		}
		
		pa.stroke(0,0,0);
//		pa.translate(0,0,-10);
		pa.stroke(0,255,0);
		pa.strokeWeight(2);
		for (Spring s:allSprings) {
			pa.line(s.getOneEnd().position().x(), s.getOneEnd().position().y(),
					s.getTheOtherEnd().position().x(), s.getTheOtherEnd().position().y());
		}
		
		pa.popMatrix();
		
		trace.beginDraw();
		trace.stroke(0);
		trace.fill(0);
		trace.translate(offsetX, offsetY);
		trace.scale(xScale, yScale);
		trace.point(G.position().x(), G.position().y());
		trace.endDraw();
		
		
				
		
	}
	
	public void moveLeg() {
		rotation -= 0.1f;
		rotate(A, rotationCenter, rotationDistance, rotation);
	}

	public void removeFromParticleSystem(ParticleSystem physics) {
		for (Particle p:allParticles) {
			//physics.
//			physics.part
			physics.removeParticle(p);
		}
		for (Spring s:allSprings) {
			physics.removeSpring(s);
		}
		
	}

	@Override
	public void createStructureFromDNA() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
