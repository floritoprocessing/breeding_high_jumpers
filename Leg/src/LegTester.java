import processing.core.PApplet;
import processing.core.PGraphics;
import traer.physics.Particle;
import traer.physics.ParticleSystem;


public class LegTester extends PApplet {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"LegTester"});
	}
	
	PGraphics trace;
	
	ParticleSystem physics;// = new ParticleSystem(0, 0.5f);
	final float mass = 1;
	//Particle rotationCenter, A, B, C, D, E, F, G;
	
	final int nrX = 5;
	final int nrY = 4;
	Leg[] leg;// = new Leg[nrX*nrY];
	
	//final float positionVariance = 5;
	
	boolean saveMovie = true;
	
	public void settings() {
		size(1024, 768,P3D);
	}
	
	public void setup() {
		
		smooth();
		resetAll();
		noCursor();
	}
	
	
	public void resetAll() {
		
		physics = new ParticleSystem(0, 0.5f);
		leg = new Leg[nrX*nrY];
		//leg = new Leg();
		
		Leg oneLeg = createParticlesAndDistances(null, 0);
		for (int i=0;i<leg.length;i++) {
			leg[i] = createParticlesAndDistances(oneLeg, 50);
		}
		oneLeg.removeFromParticleSystem(physics);
		//createParticlesAndDistances(false);
		
		//makeSprings();
				
		trace = createGraphics(width, height, P3D);
		trace.beginDraw();
		trace.background(255);
		trace.endDraw();
	}

	

	private Leg createParticlesAndDistances(Leg baseLeg, float positionVariance) {
		if (baseLeg==null) {
			
			/*while (physics.numberOfParticles()>0) {
				physics.removeParticle(physics.getParticle(0));
			}*/
			
			float rotationDistance = 25;
			Particle rotationCenter = physics.makeParticle(mass, 0, 0, 0);
			Particle A = physics.makeParticle(mass, 0, 0, 0);
			
			int xOff = 0;
			int yOff = 0;
			Particle B = physics.makeParticle(mass, -50+xOff, -50+yOff, 0);
			Particle C = physics.makeParticle(mass, -100+xOff, -40+yOff, 0);
			Particle D = physics.makeParticle(mass, -75+xOff, 0+yOff, 0);
			Particle E = physics.makeParticle(mass, -100+xOff, 25+yOff, 0);
			Particle F = physics.makeParticle(mass, -50+xOff, 50+yOff, 0);
			Particle G = physics.makeParticle(mass, -100+xOff, 100+yOff, 0);
			
			return new Leg(
					physics,
					rotationDistance
					,new Particle[] {
					rotationCenter, A, B, C, D, E, F, G
			});
		} else {
			
			float rotationDistance = Math.max(5, baseLeg.rotationDistance + gauss(positionVariance));
			
			Particle rotationCenter = physics.makeParticle(mass, 0, 0, 0);
			float[] p = positionVariance(baseLeg.A, positionVariance);
			Particle newA = physics.makeParticle(mass, p[0],p[1], 0);
			p = positionVariance(baseLeg.B, positionVariance);
			
			float xOff = gauss(positionVariance);
			float yOff = gauss(positionVariance);
			Particle newB = physics.makeParticle(mass, p[0]+xOff, p[1]+yOff, 0);
			p = positionVariance(baseLeg.C, positionVariance);
			Particle newC = physics.makeParticle(mass, p[0]+xOff, p[1]+yOff, 0);
			p = positionVariance(baseLeg.D, positionVariance);
			Particle newD = physics.makeParticle(mass, p[0]+xOff, p[1]+yOff, 0);
			p = positionVariance(baseLeg.E, positionVariance);
			Particle newE = physics.makeParticle(mass, p[0]+xOff, p[1]+yOff, 0);
			p = positionVariance(baseLeg.F, positionVariance);
			Particle newF = physics.makeParticle(mass, p[0]+xOff, p[1]+yOff, 0);
			p = positionVariance(baseLeg.G, positionVariance);
			Particle newG = physics.makeParticle(mass, p[0]+xOff, p[1]+yOff, 0);
			
			return new Leg(
					physics,
					rotationDistance,
					new Particle[] {
							rotationCenter,
							newA, newB, newC, newD, newE, newF, newG
					});

		}
		
	}
	
	private static float[] positionVariance(Particle p, float range) {
		return new float[] {
			p.position().x() + gauss(range),
			p.position().y() + gauss(range)
		};
	}

	private static float gauss(float plusMinusValue) {
		float gauss = (float)((Math.random()+Math.random()+Math.random()+Math.random()+Math.random()+Math.random()-3.0)/3.0);
		return gauss*plusMinusValue;
	}
	
	

	
	
	public void mousePressed() {
		
		// clear trace
		/*trace.beginDraw();
		trace.noStroke();
		trace.fill(255,128);
		trace.rect(0,0,width,height);
		trace.endDraw();*/
		
		resetAll();
		
		// create a new Leg based on the old one:
		//Leg newLeg = createParticlesAndDistances(leg, 1);
		
		// remove all of leg from the particle system
		//leg.removeFromParticleSystem();
		
		//leg = newLeg;
	}
	
	public void draw() {
		if (frameCount==1) {
			frame.setLocation(1920-3, -24);
		}
		
		image(trace,0,0);
		
		int i=0;
		for (int y=0;y<nrY;y++) {
			float offsetY = (float)((y+0.5)/nrY*width)*0.7f;
			for (int x=0;x<nrX;x++) {
				float offsetX = (float)((x+0.5)/nrX*height)*1.4f;
				float xScale = 2.0f/nrX * 1.5f;
				float yScale = 2.0f/nrY * 1.5f;
				float scale = Math.min(xScale, yScale);
				leg[i].drawLeg(this, offsetX, offsetY, scale, scale, trace);
				leg[i].moveLeg();
				i++;
			}
		}
		
		if (frameCount%25==0) {
			trace.beginDraw();
			trace.noStroke();
			trace.fill(255,32);
			trace.rect(0,0,width,height);
			trace.endDraw();
		}
		//leg.drawLeg(this, width/2, height/2, trace);
		
		//leg.moveLeg();
		physics.tick();
		
		if (saveMovie) {
			saveFrame("output/LegTester_####.jpg");
		}
	}
	
	

}
