package florito.genetics;

public abstract class Primitive {

	private static void catchArrayErrors(byte[] byteArray, int size) {
		if (byteArray==null) {
			throw new RuntimeException("byteArray is not allowed to be null");
		}
		else if (byteArray.length!=size) {
			throw new RuntimeException("Illegal length of byteArray ("+byteArray.length+"). Should be "+size);
		}
	}
	
	public static class Boolean implements Breedable {
		
		private static int SIZE = 1;
		private boolean value = false;
		
		public Boolean() {
		}
		
		public Boolean(boolean value) {
			this.value = value;
		}
		
		@Override
		public void insertDNA(byte[] byteArray) {
			catchArrayErrors(byteArray,SIZE);
			value = (((int)byteArray[0])&0x01)==1;
		}
		
		@Override
		public int size() {
			return 1;
		}

		@Override
		public byte[] extractDNA() {
			return new byte[] { (byte)(value?1:0) };
		}
		
		public boolean getValue() {
			return value;
		}
	}

	public static class Integer implements Breedable {
		
		private static int SIZE = 4;
		private int value = 0;
		
		public Integer() {
		}
		
		public Integer(int value) {
			this.value = value;
		}
		
		@Override
		public void insertDNA(byte[] byteArray) {
			catchArrayErrors(byteArray, SIZE);
			value = GeneticUtils.byteArrayToInt(byteArray);
			
		}

		@Override
		public int size() {
			return SIZE;
		}
		
		@Override
		public byte[] extractDNA() {
			return GeneticUtils.intToByteArray(value);
		}
		
		public int getValue() {
			return value;
		}
	}
	
	/**
	 * 32-bit single-precision Float ignoring all NaN and Infinities
	 * <p>
	 * @see http://babbage.cs.qc.cuny.edu/IEEE-754/References.xhtml
	 * <p>
	 * Use only:
	 * <p>
	 * 00000000 - (00000001-007FFFFF) - (00800000-7F7FFFFF) (+0, positive denormalized, positive normalized) <br>
	 * 80000000 - (80000001-807FFFFF) - (80800000-FF7FFFFF) (-0, negative denormalized, negative normalized)
	 * <p>
	 * <code><pre>
	 * FF000000 - FC000001	quiet -NaN				DO NOT USE
	 * FFC00000 - FC000000	indeterminate			DO NOT USE
	 * FFBFFFFF - FF800001	signaling -NaN			DO NOT USE
	 * FF800000 - FF800000	-Infinity				
	 * FF7FFFFF - 80800000	Negative normalized		
	 * 807FFFFF - 80000001	Negative denormalized	
	 * 80000000 - 80000000	Negative underflow		
	 * 80000000 - 80000000	-0
	 * 00000000 - 00000000	+0
	 * 00000000 - 00000000	Positive underflow		
	 * 00000001 - 007FFFFF	Positive denormalized	
	 * 00800000 - 7F7FFFFF	Positive normalized
	 * 7F800000 - 7F800000	+Infinity				
	 * 7F800001 - 7FBFFFFF	Signaling +NaN			DO NOT USE
	 * 7FC00000 - 7FFFFFFF	Quite +NaN				DO NOT USE
	 * </pre></code>
	 * @author Marcus
	 */
	
	public static class Float implements Breedable {

		private static int SIZE = 4;
		private float value = 0;
		
		public Float() {
		}
		
		public Float(float value) {
			this.value = value;
		}
		
		
		@Override
		public void insertDNA(byte[] byteArray) {
			catchArrayErrors(byteArray, SIZE);

			//ignore from 7F800000 - 7FFFFFFF // range 007FFFFF
			//ignore from FF800000 - FFFFFFFF
			int byte0 = (int)byteArray[0];
			byte0 += (byte0<0?256:0);
			int byte1 = (int)byteArray[1];
			byte1 += (byte1<0?256:0);
			
			if ((byte0==0x7F && byte1>=0x80)) {
				byteArray[0] = (byte)0x00;
			}
			if ((byte0==0xFF && byte1>=0x80)) {
				byteArray[0] = (byte)0x80;
			}
			
			value = GeneticUtils.byteArrayToFloat(byteArray);
		}
		
		@Override
		public int size() {
			return SIZE;
		}

		@Override
		public byte[] extractDNA() {
			byte[] out = GeneticUtils.floatToByteArray(value);
			return out;
		}
		
		public float getValue() {
			return value;
		}
		
	}
	
}
