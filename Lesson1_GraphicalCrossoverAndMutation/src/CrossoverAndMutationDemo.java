import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.URISyntaxException;

import florito.genetics.Genetics;

import processing.core.PApplet;
import processing.core.PFont;


public class CrossoverAndMutationDemo extends PApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2314755680147457497L;

//	private static boolean on2ndScreen = true;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*if (on2ndScreen) {
			PApplet.main(new String[] {"--present",
					"CrossoverAndMutationDemo"}); //,
		} else {*/
			PApplet.main(new String[] {"CrossoverAndMutationDemo"}); //"--present",
		//}
			
			
	}
	
	final float MUTATION_RATE = 0.15f;
	
	PFont font1 = null;
	SimpleNumber A, B, child;
	int[] childMutatedBits;
	
//	@Override
//	public void init() {
//		if (on2ndScreen) {
//			//frame.removeNotify();
//			//frame.setUndecorated(true);
//			//println(frame);
//		}
//		super.init();
//	}
	
	public void settings() {
		size(1024,768,JAVA2D);
	}
	
	public void setup() {
		
		//size(400,230,JAVA2D);
		smooth();
		noCursor();
//		if (on2ndScreen) {
//			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//			//frame.removeNotify();
//			//frame.setUndecorated(true);
//			//println(frame.getParent());
//			//println(frame.getLocationOnScreen());
//			//println(frame.get)
//			//frame.setLocation(300, 100);
//			//println(frame.getLocation());
//			//println(frame.getTitle());
//			//println(screenSize.width);
//			//frame.setLocation(screenSize.width,0);
//		}
		
		
//		try {
			String fontURL = "Calibri-Bold-24.vlw";//this.getClass().getResource("./Calibri-Bold-24.vlw").toURI().toString();
			font1 = loadFont(fontURL);
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
		
		textFont(font1);
		
		initValues();
	}
	
	private void initValues() {
		A = new SimpleNumber();
		B = new SimpleNumber();
		child = new SimpleNumber();
		childMutatedBits = Genetics.breed(child, MUTATION_RATE, A, B);
		//println(childMutatedBits);
	}
	
	public void mousePressed() {
		initValues();
	}
	
	
	public void draw() {
		background(240);
		
		if (frameCount==1) {
//			if (on2ndScreen) {
//				frame.setLocation(1920-2, -24);
//				//frame.removeNotify();
//				//frame.setUndecorated(true);
//			}
		}
		
		
		
		translate(0,250);
		int x0 = width/5;
		int x1 = width*2/5;
		int xArrow = width*3/5;
		int xChild = width*4/5;
		
		fill(0);
		textAlign(CENTER);
		text(A.toString(),x0,30);
		text(B.toString(),x1,30);
		text(child.toString(),xChild,30);
		
		int[] parentBits = new int[childMutatedBits.length];
		System.arraycopy(childMutatedBits, 0, parentBits, 0, childMutatedBits.length);
		for (int i=0;i<parentBits.length;i++) {
			if (parentBits[i]==0) {
				parentBits[i]=-1;
			} else {
				parentBits[i]=0;
			}
		}
		A.drawDNA(this, x0, 60, 150, parentBits);
		
		System.arraycopy(childMutatedBits, 0, parentBits, 0, childMutatedBits.length);
		for (int i=0;i<parentBits.length;i++) {
			if (parentBits[i]==1) {
				parentBits[i]=-1;
			} else {
				parentBits[i]=0;
			}
		}
		B.drawDNA(this, x1, 60, 150, parentBits);
		
		child.drawDNA(this, xChild, 60, 150, childMutatedBits);
		
		strokeWeight(3);
		stroke(0);
		line(xArrow-40,130,xArrow+40,130);
		line(xArrow+40,130,xArrow+40-30,130-15);
		line(xArrow+40,130,xArrow+40-30,130+15);
	}

	
}
