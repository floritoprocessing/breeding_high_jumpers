import processing.core.PApplet;
import processing.core.PConstants;
import florito.genetics.Breedable;
import florito.genetics.GeneticUtils;
import florito.genetics.Genetics;


public class SimpleNumber implements Breedable, PConstants {

	byte data;
	
	public SimpleNumber() {
		Genetics.randomize(this);
	}
	
	public SimpleNumber(int val) {
		data = (byte)(val&0xff);
	}
	
	@Override
	public byte[] extractDNA() {
		return new byte[] {data};
	}

	@Override
	public void insertDNA(byte[] byteArray) {
		data = byteArray[0];
	}

	@Override
	public int size() {
		return 1;
	}
	
	public int toNumber() {
		int val = (int)data;
		if (val<0) {
			val+=256;
		}
		return val;
	}
	
	public void drawDNA(PApplet pa, float x, float y, float height, int... mutatedBits) {
		String bitString = GeneticUtils.byteArrayToBitString(extractDNA());
		pa.rectMode(CENTER);
		pa.strokeWeight(1);
		int length = bitString.length();
		float rectSize = height/length;
		float y0 = y;
		for (int i=0;i<length;i++) {
			if (bitString.charAt(i)=='0') {
				pa.noFill();
			} else {
				pa.fill(128);
			}
			pa.stroke(0);
			pa.rect(x,y0,rectSize,rectSize);
			y0 += rectSize;
		}
		
		y0 = y;
		pa.noFill();
		pa.stroke(255,0,0);
		pa.strokeWeight(5);
		for (int i=0;i<length;i++) {
			if (mutatedBits!=null) {
				if (mutatedBits.length==length) {
					if (mutatedBits[i]==-1) {
						pa.rect(x,y0,rectSize,rectSize);
					}
				}
			}
			y0 += rectSize;
		}
		
		//System.out.println(bitString);
	}
	
	@Override
	public String toString() {
		return Integer.toString(toNumber());
	}

}
