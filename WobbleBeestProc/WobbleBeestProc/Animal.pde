import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * The Breedable class.<br>
 * By default all public non-final variables are the breedable ones 
 * @author Marcus
 *
 */
public abstract class Animal implements Breedable {

	private boolean breedable = true;
	
	protected ArrayList allBreedableVars;// = new ArrayList<Breedable>();
	
	public void defineBreedableVars() {
		
		allBreedableVars = new ArrayList();
		
		Class theClass = this.getClass();
		Field[] fields = theClass.getFields();
		
		ArrayList<String> acceptedVarnames = new ArrayList();
		
		for (int i=0;i<fields.length;i++) {
                  Field field= fields[i];
			Class fieldClass = field.getType();
			Class[] fieldClassInterfaces = fieldClass.getInterfaces();
			
			//System.out.println(field+ " "+fieldClass);
			for (int j=0;j<fieldClassInterfaces.length;j++) {
				
                Class fieldClassInterface = fieldClassInterfaces[j];
				/*
				 * Check if the field implements Breedable:
				 */
				if (fieldClassInterface.equals(Breedable.class)) {
					
					/*
					 * If so, check if breedable of this field is set to true
					 */
					try {
						Breedable fieldVar = (Breedable)field.get(this);
						
						/*
						 * If so, add to breedable vars
						 */
						if (fieldVar.isBreedable()) {
							allBreedableVars.add(fieldVar);
							acceptedVarnames.add(field.getName());
						}
						
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					
				}
			}
			
		}
		
		/*System.out.print(allBreedableVars.size()+" breedable vars recognized: ");
		for (String name:acceptedVarnames) {
			System.out.print(name+" ");
		}
		System.out.println();*/
	}


	public byte[] toByteArray() {
		
		
		byte[][] arrs = new byte[allBreedableVars.size()][];
		int totalSize = 0;
		for (int i=0;i<allBreedableVars.size();i++) {
			arrs[i] = ((Breedable)allBreedableVars.get(i)).toByteArray();
			totalSize += arrs[i].length;
		}
		
		byte[] out = new byte[totalSize];
		int offset = 0;
		for (int i=0;i<arrs.length;i++) {
			System.arraycopy(arrs[i], 0, out, offset, arrs[i].length);
			offset += arrs[i].length;
		}
		
		return out;
	}


	public void fromByteArray(byte[] byteArray) {
		
		byte[][] arrs = new byte[allBreedableVars.size()][];
		int totalSize = 0;
		//int[] arrayOffset = new int[allBreedableVars.size()];
		int[] arrayLength = new int[allBreedableVars.size()];
		for (int i=0;i<allBreedableVars.size();i++) {
			arrs[i] = ((Breedable)allBreedableVars.get(i)).toByteArray();
			//arrayOffset[i] = totalSize;
			arrayLength[i] = arrs[i].length;
			totalSize += arrs[i].length;
		}
		
		int srcPos = 0;
		for (int i=0;i<allBreedableVars.size();i++) {
			byte[] subByteArray = new byte[arrayLength[i]]; 
			System.arraycopy(byteArray, srcPos, subByteArray, 0, arrayLength[i]);
			((Breedable)allBreedableVars.get(i)).fromByteArray(subByteArray);
			srcPos += subByteArray.length;
		}
		
	}


	public boolean isBreedable() {
		return breedable;
	}


	public void setBreedable(boolean breedable) {
		this.breedable = breedable;
	}
	
	
	
}
