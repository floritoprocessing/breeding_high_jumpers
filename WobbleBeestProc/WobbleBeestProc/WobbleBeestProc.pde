int w=10;
	int h=20;
	Wobblebeest[] generation = new Wobblebeest[w*h];
	
	private final ParticleSystem physics = new ParticleSystem(0.1f,0.01f);
	
	int winnersForNextGeneration = 10;
	int randomForNextGeneration = 1;
	
	ArrayList winners = new ArrayList();
	
	int days = 0;

void setup() {
		size(800,800,P3D);
	
		for (int i=0;i<generation.length;i++) {
			generation[i] = new Wobblebeest(physics);
			generation[i].randomize();
		}
		
	}
	
void draw() {
		background(240);
		
		int i=0;
		float largest = Math.max(w,h);
		for (int y=0;y<h;y++) {
			for (int x=0;x<w;x++) {
				
				pushMatrix();
				translate((x+0.5f)/w*width,(y+0.5f)/h*height);
				
				scale(3f/largest, 3f/largest); //was 1.2
				generation[i].draw(this,-500,500);
				popMatrix();
				
				i++;
			}
		}
		
		for (i=0;i<generation.length;i++) {
			generation[i].tick(5.0f);
		}
		physics.tick();
		
		for (i=0;i<generation.length;i++) {
			boolean stopped = generation[i].stopIfOutside(500);
			if (stopped) {
				//System.out.println("Animal "+i+" has won ("+(winners.size()+1)+"/"+winnersForNextGeneration+")");
				winners.add(generation[i]);
			}
		}
		
		days++;
		
		if (winners.size()>=winnersForNextGeneration) {
			createNewGeneration();
		}
	}
	
	private void createNewGeneration() {
		
		System.out.println("Creating new generation after "+days+" days.");
		days = 0;

		/*
		 * Add random creatures to winners
		 */
		if (randomForNextGeneration>0) {
			for (int i=0;i<randomForNextGeneration;i++) {
				int index = (int)(Math.random()*generation.length);
				winners.add(generation[index]);
			}
		}
		
		/*
		 * Remove old generation
		 */
		for (int i=0;i<generation.length;i++) {
                  Wobblebeest beest = generation[i];
			beest.removeFromPhysics();
		}
		
		
		/*
		 * Create new generation
		 */
		Wobblebeest[] nextGeneration = new Wobblebeest[w*h];
		for (int i=0;i<nextGeneration.length;i++) {			
			Wobblebeest child = new Wobblebeest(physics);
			Wobblebeest parent0 = (Wobblebeest)winners.get((int)(Math.random()*winners.size()));
			Wobblebeest parent1 = parent0;
			while (parent1==parent0) {
				parent1 = (Wobblebeest)winners.get((int)(Math.random()*winners.size()));
			}
			
			//System.out.println("------------");
			
			Genetics.breed(child, 0.01f, new Animal[] {parent0,parent1});
			child.createPhysics();
			//System.out.println("parent0: "+Genetics.asBinaryString(parent0));
			//System.out.println("parent1: "+Genetics.asBinaryString(parent1));		
			//System.out.println("child:   "+Genetics.asBinaryString(child));
			
			nextGeneration[i] = child;
		}
		
		/*
		 * Replace old generation
		 */
		generation = nextGeneration;
		
		winners.clear();
		
	}
