public static class Genetics {

	public static void breed(Animal child, float mutationRate, Animal[] parents) {
		if (child==null) {
			throw new RuntimeException("Cannot make a child that is null");
		}
		if (parents==null || parents.length==0) {
			throw new RuntimeException("Need at least one parent");
		}
		
		child.defineBreedableVars();
		for (int i=0;i<parents.length;i++) {
			parents[i].defineBreedableVars();
		}
		
		// create parent byte arrays
		byte[][] parentByteArray = new byte[parents.length][];
		for (int i=0;i<parents.length;i++) {
			parentByteArray[i] = parents[i].toByteArray();
		}
		
		// create bit arrays
		boolean[][] parentBitArray = new boolean[parents.length][parentByteArray[0].length*8];
		for (int i=0;i<parents.length;i++) {
			int bit = 0;
			for (int j=0;j<parentByteArray[i].length;j++) {
				byte b = parentByteArray[i][j];
				parentBitArray[i][bit++] = ((b>>7)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>6)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>5)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>4)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>3)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>2)&0x01)==1;
				parentBitArray[i][bit++] = ((b>>1)&0x01)==1;
				parentBitArray[i][bit++] = (b&0x01)==1;
			}
		}
		
		/*for (int i=0;i<parentBitArray.length;i++) {
			System.out.print("parentbits: ");
			for (int j=0;j<parentBitArray[i].length;j++) {
				System.out.print(parentBitArray[i][j]?"1":"0");
			}
			System.out.println();
		}*/
		
		// cross and breed
		boolean[] childBitArray = new boolean[parentBitArray[0].length]; 
		for (int i=0;i<parentBitArray[0].length;i++) {
			int selectedParent = (int)(Math.random()*parents.length);
			childBitArray[i] = parentBitArray[selectedParent][i];
			if (mutationRate>0 && Math.random()<mutationRate) {
				childBitArray[i] = !childBitArray[i];
			}
		}
		
		/*System.out.print("childbits:  ");
		for (int j=0;j<childBitArray.length;j++) {
			System.out.print(childBitArray[j]?"1":"0");
		}
		System.out.println();*/
		
		
		// create byte array
		byte[] childByteArray = new byte[childBitArray.length/8];
		int bit=0;
		for (int i=0;i<childByteArray.length;i++) {
			int b=0;
			b = b | (childBitArray[bit++]?0x80:0);
			b = b | (childBitArray[bit++]?0x40:0);
			b = b | (childBitArray[bit++]?0x20:0);
			b = b | (childBitArray[bit++]?0x10:0);
			b = b | (childBitArray[bit++]?0x08:0);
			b = b | (childBitArray[bit++]?0x04:0);
			b = b | (childBitArray[bit++]?0x02:0);
			b = b | (childBitArray[bit++]?0x01:0);
			childByteArray[i] = (byte)b;
		}
		
		child.fromByteArray(childByteArray);
	}

	
	public static String asBinaryString(Breedable breedable) {
		return asBinaryString(breedable.toByteArray());
	}
	
	public static String asBinaryString(byte[] byteArray) {
		StringBuilder sb = new StringBuilder();
		int len = byteArray.length;
		int len1 = len-1;
		for (int j=0;j<len;j++) {
			int val = (int)byteArray[j];
			if (val<0) {
				val+=256;
			}
			String binString = Integer.toBinaryString(val);
			while (binString.length()<8) {
				binString = "0"+binString;
			}
			sb.append(binString);
			if (j<len1) {
				sb.append(" ");
			}
		}		
		return sb.toString();
	}
	
}
