public interface Breedable {

	public void setBreedable(boolean breedable);
	public boolean isBreedable(); 
	public byte[] toByteArray();
	public void fromByteArray(byte[] byteArray);
	public void randomize();
	
}

