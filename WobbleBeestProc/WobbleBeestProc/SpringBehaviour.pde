/**
 * LegSize: 0..360
 * @author Marcus
 *
 */
public static class SpringBehaviour implements Breedable {

	private static float twoByteToDegree = 360.0f/65536.0f;
	private static float degreeToTwoByte = 65536.0f/360.0f;
	
	private static float minLength = 10.0f;
	private static float twoByteToLength = 90.0f/65535f;
	private static float lengthToTwoByte = 65535f/90.0f;
		
	private static float twoByteToLengthVariation = 1.0f/65535f;
	private static float lengthVariationToTwoByte = 65535f/1.0f;
	
	private static float degreeToRadians = (float)(Math.PI/180.0);
	
	//private float temporalLength = 0;
	private float temporalPhase = 0;
	
	private float phase = 0;
	private float speed = 0;
	private float length = 0;
	private float lengthVariation = 0;
	
	public SpringBehaviour() {
	}
	
	public void tick(float fac) {
		temporalPhase += speed*fac;
		if (temporalPhase>=360) {
			temporalPhase-=360;
		}
		if (temporalPhase<0) {
			temporalPhase+=360;
		}
		//System.out.println("temporalPhase: "+temporalPhase);
	}

	public float calculateTemporalLength() {
		return length + (length*lengthVariation)*(float)Math.sin(temporalPhase*degreeToRadians);
	}
	
	public float getPhase() {
		return phase;
	}
	
	public float getSpeed() {
		return speed;
	}
	
	public float getLength() {
		return length;
	}
	
	public float getLengthVariation() {
		return lengthVariation;
	}
	

	public void randomize() {
		byte[] byteArray = new byte[] {
			(byte)(Math.random()*256),
			(byte)(Math.random()*256),
			(byte)(Math.random()*256),
			(byte)(Math.random()*256),
			(byte)(Math.random()*256),
			(byte)(Math.random()*256),
			(byte)(Math.random()*256),
			(byte)(Math.random()*256)
		};
		fromByteArray(byteArray);
	}
	

	public void fromByteArray(byte[] byteArray) {
		int MSB = (int)byteArray[0];
		int LSB = (int)byteArray[1];
		if (MSB<0) {
			MSB += 256;
		}
		if (LSB<0) {
			LSB += 256;
		}
		int twoByte = MSB<<8|LSB; // 0..65536
		phase = twoByte*twoByteToDegree;	
		
		MSB = (int)byteArray[2];
		LSB = (int)byteArray[3];
		if (MSB<0) {
			MSB += 256;
		}
		if (LSB<0) {
			LSB += 256;
		}
		twoByte = MSB<<8|LSB; // 0..65536
		speed = (1+twoByte*twoByteToDegree)*0.01f;	 //0.01..3.6
		
		MSB = (int)byteArray[4];
		LSB = (int)byteArray[5];
		if (MSB<0) {
			MSB += 256;
		}
		if (LSB<0) {
			LSB += 256;
		}
		twoByte = MSB<<8|LSB; // 0..65536
		length = minLength+(twoByte*twoByteToLength);
		
		MSB = (int)byteArray[6];
		LSB = (int)byteArray[7];
		if (MSB<0) {
			MSB += 256;
		}
		if (LSB<0) {
			LSB += 256;
		}
		twoByte = MSB<<8|LSB; // 0..65536
		lengthVariation = (twoByte*twoByteToLengthVariation);
		
		temporalPhase = phase;
	}
	

	public byte[] toByteArray() {
		int twoByte = (int)Math.max(0,Math.min(65535,phase*degreeToTwoByte));
		int MSBPhase = twoByte>>8&0xff;
		int LSBPhase = twoByte&0xff;
		
		float val = (speed*100)-1;
		twoByte = (int)Math.max(0,Math.min(65535,val*degreeToTwoByte));
		int MSBSpeed = twoByte>>8&0xff;
		int LSBSpeed  = twoByte&0xff;
		
		val = (length-minLength);
		twoByte = (int)Math.max(0,Math.min(65535,val*lengthToTwoByte));
		int MSBLength = twoByte>>8&0xff;
		int LSBLength = twoByte&0xff;
		
		twoByte = (int)Math.max(0,Math.min(65535,lengthVariation*lengthVariationToTwoByte));
		int MSBLengthVar = twoByte>>8&0xff;
		int LSBLengthVar = twoByte&0xff;
		
		return new byte[] {
				(byte)MSBPhase, (byte)LSBPhase, 
				(byte)MSBSpeed, (byte)LSBSpeed, 
				(byte)MSBLength, (byte)LSBLength,
				(byte)MSBLengthVar, (byte)LSBLengthVar};
	}


	public boolean isBreedable() {
		return true;
	}


	public void setBreedable(boolean breedable) {
	}
	

	public String toString() {
		return "Length: "+length+" (lengthVar: "+lengthVariation+") (phase: "+phase+", speed: "+speed+")";
	}

	

}
